package com.app.vitualtour.model.htmldata;

import java.io.Serializable;

public class Scenes implements Serializable
{
    private ScreenName screenName;

    public ScreenName getScreenName() {
        return screenName;
    }

    public void setScreenName(ScreenName screenName) {
        this.screenName = screenName;
    }
}