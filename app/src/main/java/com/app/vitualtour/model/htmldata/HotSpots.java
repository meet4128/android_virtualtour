package com.app.vitualtour.model.htmldata;

import java.io.Serializable;

public class HotSpots implements Serializable
{


    private String type;
    private String text;
    private String sceneId;

    private int pitch;
    private int yaw;

    private int targetYaw;

    private int targetPitch;

    public String getSceneId ()
    {
        return sceneId;
    }

    public void setSceneId (String sceneId)
    {
        this.sceneId = sceneId;
    }

    public int getPitch ()
    {
        return pitch;
    }

    public void setPitch (int pitch)
    {
        this.pitch = pitch;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public int getYaw ()
    {
        return yaw;
    }

    public void setYaw (int yaw)
    {
        this.yaw = yaw;
    }

    public int getTargetYaw() {
        return targetYaw;
    }

    public void setTargetYaw(int targetYaw) {
        this.targetYaw = targetYaw;
    }

    public int getTargetPitch() {
        return targetPitch;
    }

    public void setTargetPitch(int targetPitch) {
        this.targetPitch = targetPitch;
    }
}
			
			