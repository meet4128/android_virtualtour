package com.app.vitualtour.model.htmldata;

import java.io.Serializable;

public class Default implements Serializable
{
    private String firstScene;

    private boolean autoLoad;

    private int sceneFadeDuration;

    public String getFirstScene ()
    {
        return firstScene;
    }

    public void setFirstScene (String firstScene)
    {
        this.firstScene = firstScene;
    }

    public boolean getAutoLoad ()
    {
        return autoLoad;
    }

    public void setAutoLoad (boolean autoLoad)
    {
        this.autoLoad = autoLoad;
    }

    public int getSceneFadeDuration ()
    {
        return sceneFadeDuration;
    }

    public void setSceneFadeDuration (int sceneFadeDuration)
    {
        this.sceneFadeDuration = sceneFadeDuration;
    }
}