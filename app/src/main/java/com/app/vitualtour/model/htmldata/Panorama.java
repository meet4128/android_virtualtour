package com.app.vitualtour.model.htmldata;

import java.io.Serializable;

public class Panorama implements Serializable
{
    private Default defaultData;

    private Scenes scenes;

    public Default getDefault ()
    {
        return defaultData;
    }

    public void setDefault (Default defaultData)
    {
        this.defaultData = defaultData;
    }

    public Scenes getScenes ()
    {
        return scenes;
    }

    public void setScenes (Scenes scenes)
    {
        this.scenes = scenes;
    }
}