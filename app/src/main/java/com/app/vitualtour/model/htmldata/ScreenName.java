package com.app.vitualtour.model.htmldata;

import java.io.Serializable;

public class ScreenName implements Serializable
{
    private HotSpots[] hotSpots;

    private String panorama;

    private String hfov;

    private String pitch;

    private String title;

    private String type;

    private String yaw;

    public HotSpots[] getHotSpots ()
    {
        return hotSpots;
    }

    public void setHotSpots (HotSpots[] hotSpots)
    {
        this.hotSpots = hotSpots;
    }

    public String getPanorama ()
    {
        return panorama;
    }

    public void setPanorama (String panorama)
    {
        this.panorama = panorama;
    }

    public String getHfov ()
    {
        return hfov;
    }

    public void setHfov (String hfov)
    {
        this.hfov = hfov;
    }

    public String getPitch ()
    {
        return pitch;
    }

    public void setPitch (String pitch)
    {
        this.pitch = pitch;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getYaw ()
    {
        return yaw;
    }

    public void setYaw (String yaw)
    {
        this.yaw = yaw;
    }

}