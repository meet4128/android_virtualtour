package com.app.vitualtour.model;

import android.graphics.PointF;

public class LinesResult {
	boolean isContain;
	PointF mCurrentPoint;
	String lineColor;

	public String getLineColor() {
		return lineColor;
	}

	public void setLineColor(String lineColor) {
		this.lineColor = lineColor;
	}

	public boolean isContain() {
		return isContain;
	}

	public void setContain(boolean contain) {
		isContain = contain;
	}

	public PointF getmCurrentPoint() {
		return mCurrentPoint;
	}

	public void setmCurrentPoint(PointF mCurrentPoint) {
		this.mCurrentPoint = mCurrentPoint;
	}
}
