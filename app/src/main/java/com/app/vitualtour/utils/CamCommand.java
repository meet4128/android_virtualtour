package com.app.vitualtour.utils;

public class CamCommand
{
    public static final String ms_ip = "192.168.42.1";
    public static final int ms_tcp_port = 7878;
    public static final int ms_uk_port = 8787;
    public static final int ms_fs_port = 50422;
    public static final int CONNECT = 257;
    public static final int DISCONNECT = 258;
    public static final int GET_SERIAL_NO = 4097;
    public static final int GET_WIFI = 1539;
    public static final int REGISTER_TCP = 261;//  # {"msg_id":261,"param":"192.168.42.2","token":3,"type":"TCP"}
    public static final int GET_CAMERA = 4365;
    public static final int SET_DATE = 6147;//  # {"msg_id":6147,"param":"2017-12-16-12-00-04-6","token":3}
    public static final int GET_STORAGE = 4358;
    public static final int GET_DEVICE = 4364;
    public static final int CHANGE_DIRECTORY = 1283; // # {"msg_id":1283,"param":"/tmp/SD0/DCIM/","token":2}
    public static final int LIST_DIRECTORY = 1282;
    public static final int SET_GPS = 4917;//  # alt, lat, lon

    public static final int  SWITCH_MODE = 4611;//  # 1 for camera, 0 for recorder
    public static final int SET_PHOTO_TIMER = 4866;  //# 0, 3, 4, 5, 6, 7, 8, 9, 10
    public static final int SET_PHOTO_INTERVALOMETER = 4929;//  # 0, 2, 5, 10, 30, 60, 120, 300
    public static final int SET_PHOTO_BRACKETING = 4918;//  # if +-x on app, param = 2 * x

     public static final int SET_VIDEO_SHORT = 4618 ;// # 0, 10, 20, 30 seconds
     public static final int SET_VIDEO_TIMELAPSE = 4621;//  # 0, 255, 1, 2, 5, 10, 30, 60 seconds
     public static final int SET_VIDEO_SLOWMO = 4916;//  # 0, 2 times; 4, 8 are not supported

   public static final int SET_BUZZER_VOLUME = 6145 ; //# 0, 1, 2, 3, 4 -> [0, 100]
   public static final int SET_LED = 6156;  //# 0, 1
   public static final int AUTO_TURN_OFF = 6151 ;// # params 0, 5, 10 seconds
   public static final int TURN_OFF = 6155;
   public static final int DELETE_FILE = 1281;
   public static final int FILE_INFO = 1026;

    //    # {"msg_id":1025,"param":"/tmp/SD0/DCIM/20171216/IMG_20171216_030505.JPG","token":3,"type":"thumb"}
//# {"msg_id":1025,"param":"/tmp/SD0/DCIM/20171216/IMG_20171216_030505.MP4","token":3,"type":"idr"}
  public static final int  FILE_PREVIEW = 1025;
  public static final int  GYRO_CALIBRATION = 6165;

  public static final int  PHOTO_PRESS = 4864 ;// # was old 513 ?
  public static final int  PHOTO_LONGPRESS = 4868; //# timer

  public static final int      VIDEO_REC_START = 5028;
  public static final int  VIDEO_REC_STOP = 514  ;//#
  public static final int  VIDEO_LONGPRESS = 4623;

       // # 6912x3456: 0, 3456x1728(stitched): 1, 6912x3456(stitched): 2, 6912x3456(RAW): 3
   public static final int  PHOTO_RES = 4872;
   public static final int  PHOTO_WB = 5168 ; //# Auto: 0, Outdoors: 1, Overcast: 2, Incandescent: 3, Fluorescent: 4
   public static final int  PHOTO_EV = 5169 ; //# if x[-3, 3, .5] on app, param = 2 * x
   public static final int  PHOTO_SHUTTERTIME = 5171 ;// # if auto; x = 0, if x >= 1; x, if x < 1; (1 << 15) + 1/x
    public static final int    PHOTO_ISO = 5172 ; //# 0, 50, 100, 200, 400, 800, 1600

       // # 3456x1728|30fps: 0, 2304x1152|30fps: 1, 2304x1152|60fps: 2, 2048x512|120fps Top: 7, 2048x512|120fps bottom (Bullet time): 9, 3456x1728|30fps HighBitrate: 10
  public static final int  VIDEO_RES = 4612;
  public static final int  VIDEO_WB = 5136 ; //# Auto: 0, Outdoors: 1, Overcast: 2, Incandescent: 3, Fluorescent: 4
  public static final int  VIDEO_EV = 5137;  //# if x[-3, 3, .5] on app, param = 2 * x

//# responses
//# values for charging and discharging. is value for charging is twice of actual value?
public static final int  BATTERY_STATUS = 4361;
    public static final int    STANDBY = 4362;//  # enabled 1, disabled 0

  /*      # unknown
# 1793, 6162

        # from Xiaomi_yi
# 3 - video settings and camera clock
# 11 - device details*/
}
