package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;

public class AMBAShootPhotoIntervalResponse extends AMBAResponse {
  @Expose
  private int time;
  
  public int getTime() {
    return this.time;
  }
  
  public void setTime(int paramInt) {
    this.time = paramInt;
  }
}


/* Location:              /Users/makavanadhaiyur/Documents/Dhaiyur/dex2jar/classes10-dex2jar.jar!/com/madv360/madv/connection/ambaresponse/AMBAShootPhotoIntervalResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */