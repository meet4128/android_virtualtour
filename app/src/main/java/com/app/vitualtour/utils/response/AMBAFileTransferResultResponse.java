package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;

import java.util.ArrayList;
import java.util.Map;

public class AMBAFileTransferResultResponse extends AMBAResponse {
  public long getBytesReceived() {
    Double double_ = (Double)((Map)((ArrayList<Map>)this.param).get(0)).get("bytes received");
    if (double_ == null) {
      double d1 = 0.0D;
      return (long)d1;
    } 
    double d = double_.doubleValue();
    return (long)d;
  }
  
  public long getBytesSent() {
    Double double_ = (Double)((Map)((ArrayList<Map>)this.param).get(0)).get("bytes sent");
    if (double_ == null) {
      double d1 = 0.0D;
      return (long)d1;
    } 
    double d = double_.doubleValue();
    return (long)d;
  }
  
  public String getMD5() {
    return (String)((Map)((ArrayList<Map>)this.param).get(1)).get("md5sum");
  }
}
