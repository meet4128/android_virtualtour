package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;


public class AMBAGetThumbnailResponse extends AMBAResponse {
  @Expose
  public String md5sum;
  
  @Expose
  public int size;
}
