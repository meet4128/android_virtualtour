package com.app.vitualtour.utils.request;

import com.app.vitualtour.utils.AMBARequest;
import com.app.vitualtour.utils.response.AMBAPutFileResponse;
import com.google.gson.annotations.Expose;

public class AMBAPutFileRequest extends AMBARequest {
  @Expose
  public int fType;
  
  @Expose
  public String md5sum;
  
  @Expose
  public long offset;
  
  @Expose
  public long size;
  
  public AMBAPutFileRequest(AMBARequest.ResponseListener paramResponseListener) {
    super(paramResponseListener, AMBAPutFileResponse.class);
  }
}
