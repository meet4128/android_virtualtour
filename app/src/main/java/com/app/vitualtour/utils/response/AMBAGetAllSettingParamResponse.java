package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;


public class AMBAGetAllSettingParamResponse extends AMBAResponse {
  @Expose
  private int battery;
  
  @Expose
  private int buzzer;
  
  @Expose
  private int cam_jpg;
  
  @Expose
  private int cam_mp4;
  
  @Expose
  private int led;
  
  @Expose
  private int loop;
  
  @Expose
  private String md5;
  
  @Expose
  private int p_res;
  
  @Expose
  private int pano_sound;
  
  @Expose
  private int poweroff_en;
  
  @Expose
  private int poweroff_time;
  
  @Expose
  private String product;
  
  @Expose
  private String sn;
  
  @Expose
  private int standby_en;
  
  @Expose
  private int standby_time;
  
  @Expose
  private int still_ev;
  
  @Expose
  private int still_iso;
  
  @Expose
  private int still_shutter;
  
  @Expose
  private int still_wb;
  
  @Expose
  private int v_res;
  
  @Expose
  private String ver;
  
  @Expose
  private int video_ev;
  
  @Expose
  private int video_iso;
  
  @Expose
  private int video_shutter;
  
  @Expose
  private int video_wb;
  
  public int getBattery() {
    return this.battery;
  }
  
  public int getBuzzer() {
    return this.buzzer;
  }
  
  public int getCam_jpg() {
    return this.cam_jpg;
  }
  
  public int getCam_mp4() {
    return this.cam_mp4;
  }
  
  public int getLed() {
    return this.led;
  }
  
  public int getLoop() {
    return this.loop;
  }
  
  public String getMd5() {
    return this.md5;
  }
  
  public int getP_res() {
    return this.p_res;
  }
  
  public int getPano_sound() {
    return this.pano_sound;
  }
  
  public int getPoweroff_en() {
    return this.poweroff_en;
  }
  
  public int getPoweroff_time() {
    return this.poweroff_time;
  }
  
  public String getProduct() {
    return this.product;
  }
  
  public String getSn() {
    return this.sn;
  }
  
  public int getStandby_en() {
    return this.standby_en;
  }
  
  public int getStandby_time() {
    return this.standby_time;
  }
  
  public int getStill_ev() {
    return this.still_ev;
  }
  
  public int getStill_iso() {
    return this.still_iso;
  }
  
  public int getStill_shutter() {
    return this.still_shutter;
  }
  
  public int getStill_wb() {
    return this.still_wb;
  }
  
  public int getV_res() {
    return this.v_res;
  }
  
  public String getVer() {
    return this.ver;
  }
  
  public int getVideo_ev() {
    return this.video_ev;
  }
  
  public int getVideo_iso() {
    return this.video_iso;
  }
  
  public int getVideo_shutter() {
    return this.video_shutter;
  }
  
  public int getVideo_wb() {
    return this.video_wb;
  }
  
  public void setBattery(int paramInt) {
    this.battery = paramInt;
  }
  
  public void setBuzzer(int paramInt) {
    this.buzzer = paramInt;
  }
  
  public void setCam_jpg(int paramInt) {
    this.cam_jpg = paramInt;
  }
  
  public void setCam_mp4(int paramInt) {
    this.cam_mp4 = paramInt;
  }
  
  public void setLed(int paramInt) {
    this.led = paramInt;
  }
  
  public void setLoop(int paramInt) {
    this.loop = paramInt;
  }
  
  public void setMd5(String paramString) {
    this.md5 = paramString;
  }
  
  public void setP_res(int paramInt) {
    this.p_res = paramInt;
  }
  
  public void setPano_sound(int paramInt) {
    this.pano_sound = paramInt;
  }
  
  public void setPoweroff_en(int paramInt) {
    this.poweroff_en = paramInt;
  }
  
  public void setPoweroff_time(int paramInt) {
    this.poweroff_time = paramInt;
  }
  
  public void setProduct(String paramString) {
    this.product = paramString;
  }
  
  public void setSn(String paramString) {
    this.sn = paramString;
  }
  
  public void setStandby_en(int paramInt) {
    this.standby_en = paramInt;
  }
  
  public void setStandby_time(int paramInt) {
    this.standby_time = paramInt;
  }
  
  public void setStill_ev(int paramInt) {
    this.still_ev = paramInt;
  }
  
  public void setStill_iso(int paramInt) {
    this.still_iso = paramInt;
  }
  
  public void setStill_shutter(int paramInt) {
    this.still_shutter = paramInt;
  }
  
  public void setStill_wb(int paramInt) {
    this.still_wb = paramInt;
  }
  
  public void setV_res(int paramInt) {
    this.v_res = paramInt;
  }
  
  public void setVer(String paramString) {
    this.ver = paramString;
  }
  
  public void setVideo_ev(int paramInt) {
    this.video_ev = paramInt;
  }
  
  public void setVideo_iso(int paramInt) {
    this.video_iso = paramInt;
  }
  
  public void setVideo_shutter(int paramInt) {
    this.video_shutter = paramInt;
  }
  
  public void setVideo_wb(int paramInt) {
    this.video_wb = paramInt;
  }
}


/* Location:              /Users/makavanadhaiyur/Documents/Dhaiyur/dex2jar/classes10-dex2jar.jar!/com/madv360/madv/connection/ambaresponse/AMBAGetAllSettingParamResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */