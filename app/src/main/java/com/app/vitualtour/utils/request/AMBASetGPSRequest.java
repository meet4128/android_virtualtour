package com.app.vitualtour.utils.request;

import com.app.vitualtour.utils.AMBARequest;
import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;

public class AMBASetGPSRequest extends AMBARequest {
  @Expose
  public String alt;
  
  @Expose
  public String lat;
  
  @Expose
  public String lon;
  
  public AMBASetGPSRequest(AMBARequest.ResponseListener paramResponseListener) {
    super(paramResponseListener, AMBAResponse.class);
  }
}
