package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;


public class AMBACancelFileTransferResponse extends AMBAResponse {
  @Expose
  protected int transferred_size;
  
  public int getBytesSent() {
    return this.transferred_size;
  }
  
  public String getMD5() {
    return "N/A";
  }
}
