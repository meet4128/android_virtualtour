package com.app.vitualtour.utils;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


import okio.BufferedSource;
import okio.Okio;


public class CamConnection {

    String ms_ip = "192.168.42.1";
    int ms_tcp_port = 7878;
    int ms_uk_port = 8787;
    int ms_fs_port = 50422;

    private InputStream socketInputStream = null;
    private OutputStream socketOutputStream = null;


    public CamConnection()
    {
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void init() throws IOException {
        Socket mSocket = new Socket();
        InetSocketAddress inetSocketAddress = new InetSocketAddress(ms_ip,ms_tcp_port);
        mSocket.connect(inetSocketAddress, 8000);
        mSocket.setTcpNoDelay(true);
        this.socketOutputStream = mSocket.getOutputStream();
        this.socketInputStream = mSocket.getInputStream();
    }

    public void CapturePhoto()
    {
        try{
            SendData CapturePhoto = new SendData();
            CapturePhoto.setMsg_id(4864);
            CapturePhoto.setToken(1);
            String makeRequest = CapturePhoto.objectToJson(CapturePhoto);

            socketOutputStream.write(makeRequest.getBytes(Charset.forName("UTF-8")));
            socketOutputStream.flush();
        }catch (Exception e){
            Log.d("Dhaiyur",e.toString());
        }
    }

    public String listingPhoto()
    {
        try{
            SendData getlist = new SendData();
            getlist.setMsg_id(1282);
            getlist.setToken(1);
            String getListingResult = getlist.objectToJson(getlist);

            socketOutputStream.write(getListingResult.getBytes(StandardCharsets.UTF_8));
            socketOutputStream.flush();

            BufferedSource fromSource = Okio.buffer(Okio.source(socketInputStream));
            fromSource.readUtf8();

            return readResponse(socketInputStream);
        }catch (Exception e){
            Log.d("Dhaiyur",e.toString());
            return "";
        }
    }


    class Client extends Thread {
        public void run()
        {
            try
            {
                BufferedReader reader = new BufferedReader(new InputStreamReader(socketInputStream, StandardCharsets.UTF_8));
                char[] buffer = new char[8092];
                StringBuilder result = new StringBuilder();

                if (reader.ready()) {
                    int charsRead = reader.read(buffer);
                    result.append(buffer, 0, charsRead);
                }
            }
            catch(Exception e) {
                Log.d("Dhaiyur",e.toString());
            }
        }
    }

    public String readResponse(InputStream inStreamFromServer) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inStreamFromServer, StandardCharsets.UTF_8));
        char[] buffer = new char[8092];
        StringBuilder result = new StringBuilder();
        if (reader.ready()) {
            int charsRead = reader.read(buffer);
            result.append(buffer, 0, charsRead);
        }
        return result.toString();
    }
}


