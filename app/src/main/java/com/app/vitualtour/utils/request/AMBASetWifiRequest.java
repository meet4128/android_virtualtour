package com.app.vitualtour.utils.request;

import com.app.vitualtour.utils.AMBARequest;
import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;

public class AMBASetWifiRequest extends AMBARequest {
  @Expose
  public String passwd;
  
  @Expose
  public String ssid;
  
  public AMBASetWifiRequest(AMBARequest.ResponseListener paramResponseListener) {
    super(paramResponseListener, AMBAResponse.class);
  }
}