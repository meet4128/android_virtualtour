package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;

public class AMBAShootPhotoSurroundExpResponse extends AMBAResponse {
  @Expose
  private int sur_exp;
  
  public int getSur_exp() {
    return this.sur_exp;
  }
  
  public void setSur_exp(int paramInt) {
    this.sur_exp = paramInt;
  }
}


/* Location:              /Users/makavanadhaiyur/Documents/Dhaiyur/dex2jar/classes10-dex2jar.jar!/com/madv360/madv/connection/ambaresponse/AMBAShootPhotoSurroundExpResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */