package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;

public class AMBAGetAllModeParamResponse extends AMBAResponse {
  @Expose
  private int burst_num;
  
  @Expose
  private int cap_interval;
  
  @Expose
  private int cap_interval_num;
  
  @Expose
  private int lapse;
  
  @Expose
  private int mode;
  
  @Expose
  private int rec_time;
  
  @Expose
  private int second;
  
  @Expose
  private int sensor_num;
  
  @Expose
  private int speedx;
  
  @Expose
  private int status;
  
  @Expose
  private int sub_mode;
  
  @Expose
  private int surexp_c;
  
  @Expose
  private int surroundexp;
  
  @Expose
  private int timing;
  
  @Expose
  private int timing_c;
  
  public int getBurst_num() {
    return this.burst_num;
  }
  
  public int getCap_interval() {
    return this.cap_interval;
  }
  
  public int getCap_interval_num() {
    return this.cap_interval_num;
  }
  
  public int getLapse() {
    return this.lapse;
  }
  
  public int getMode() {
    return this.mode;
  }
  
  public int getRec_time() {
    return this.rec_time;
  }
  
  public int getSecond() {
    return this.second;
  }
  
  public int getSensor_num() {
    return this.sensor_num;
  }
  
  public int getSpeedx() {
    return this.speedx;
  }
  
  public int getStatus() {
    return this.status;
  }
  
  public int getSub_mode() {
    return this.sub_mode;
  }
  
  public int getSurexp_c() {
    return this.surexp_c;
  }
  
  public int getSurroundexp() {
    return this.surroundexp;
  }
  
  public int getTiming() {
    return this.timing;
  }
  
  public int getTiming_c() {
    return this.timing_c;
  }
  
  public void setBurst_num(int paramInt) {
    this.burst_num = paramInt;
  }
  
  public void setCap_interval(int paramInt) {
    this.cap_interval = paramInt;
  }
  
  public void setCap_interval_num(int paramInt) {
    this.cap_interval_num = paramInt;
  }
  
  public void setLapse(int paramInt) {
    this.lapse = paramInt;
  }
  
  public void setMode(int paramInt) {
    this.mode = paramInt;
  }
  
  public void setRec_time(int paramInt) {
    this.rec_time = paramInt;
  }
  
  public void setSecond(int paramInt) {
    this.second = paramInt;
  }
  
  public void setSensor_num(int paramInt) {
    this.sensor_num = paramInt;
  }
  
  public void setSpeedx(int paramInt) {
    this.speedx = paramInt;
  }
  
  public void setStatus(int paramInt) {
    this.status = paramInt;
  }
  
  public void setSub_mode(int paramInt) {
    this.sub_mode = paramInt;
  }
  
  public void setSurexp_c(int paramInt) {
    this.surexp_c = paramInt;
  }
  
  public void setSurroundexp(int paramInt) {
    this.surroundexp = paramInt;
  }
  
  public void setTiming(int paramInt) {
    this.timing = paramInt;
  }
  
  public void setTiming_c(int paramInt) {
    this.timing_c = paramInt;
  }
  
  public String toString() {
    return "AMBAResponse(" + hashCode() + ") : rval=" + this.rval + ", msgID=" + this.msg_id + ", param=" + this.param + ", type=" + this.type + ", token=" + this.token + "AMBAGetAllModeParamResponse{status=" + this.status + ", mode=" + this.mode + ", rec_time=" + this.rec_time + ", second=" + this.second + ", lapse=" + this.lapse + ", timing=" + this.timing + ", timing_c=" + this.timing_c + ", cap_interval=" + this.cap_interval + ", cap_interval_num=" + this.cap_interval_num + ", speedx=" + this.speedx + ", surroundexp=" + this.surroundexp + ", surexp_c=" + this.surexp_c + '}';
  }
}
