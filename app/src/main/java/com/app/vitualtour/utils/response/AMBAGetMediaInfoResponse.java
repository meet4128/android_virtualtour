package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;


public class AMBAGetMediaInfoResponse extends AMBAResponse {
  @Expose
  public int duration;
  
  @Expose
  public String gyro;
  
  @Expose
  private int res_id;
  
  @Expose
  public int scene_type = 0;
  
  @Expose
  private long size;
  
  public long getSize() {
    return (this.size < 0L) ? (4294967296L + this.size) : this.size;
  }
  
  public int getVideoCaptureResolution() {
    return this.res_id;
  }
}


/* Location:              /Users/makavanadhaiyur/Documents/Dhaiyur/dex2jar/classes10-dex2jar.jar!/com/madv360/madv/connection/ambaresponse/AMBAGetMediaInfoResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */