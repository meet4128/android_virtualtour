package com.app.vitualtour.utils;

import com.google.gson.annotations.Expose;
import java.io.Serializable;

public class AMBAResponse implements Serializable {
  @Expose
  protected int msg_id;
  
  @Expose
  protected Object param;
  
  @Expose
  protected int rval;
  
  @Expose
  protected int token;
  
  @Expose
  protected String type;
  
  public static int rvalID(int paramInt) {
    return paramInt & 0xFFFFFF7F;
  }
  
  public int getMsg_id() {
    return this.msg_id;
  }
  
  public Object getParam() {
    return this.param;
  }
  
  public String getRequestKey() {
    return Integer.toString(this.msg_id);
  }
  
  public int getRval() {
    return this.rval;
  }
  
  public int getToken() {
    return this.token;
  }
  
  public String getType() {
    return this.type;
  }
  
  public boolean isRvalOK() {
    return (this.rval == 0 || this.rval == 128);
  }
  
  public void setMsg_id(int paramInt) {
    this.msg_id = paramInt;
  }
  
  public void setParam(Object paramObject) {
    this.param = paramObject;
  }
  
  public void setRval(int paramInt) {
    this.rval = paramInt;
  }
  
  public void setToken(int paramInt) {
    this.token = paramInt;
  }
  
  public void setType(String paramString) {
    this.type = paramString;
  }
  
  public String toString() {
    return "AMBAResponse(" + hashCode() + ") : rval=" + this.rval + ", msgID=" + this.msg_id + ", param=" + this.param + ", type=" + this.type + ", token=" + this.token;
  }
}
