package com.app.vitualtour.utils;

public class AMBACommands {
  public static final int AMBA_CAMERA_COMMAND_PORT = 7878;
  
  public static final int AMBA_CAMERA_DATA_PORT = 8787;
  
  public static final String AMBA_CAMERA_HTTPS_URL_ROOT = "https://192.168.42.1:443/";
  
  public static final String AMBA_CAMERA_HTTP_URL_ROOT = "http://192.168.42.1:50422/";
  
  public static final String AMBA_CAMERA_IP = "192.168.42.1";
  
  public static final String AMBA_CAMERA_RTSP_LIVE_URL = "rtsp://192.168.42.1/live";
  
  public static final String AMBA_CAMERA_RTSP_URL_ROOT = "rtsp://192.168.42.1";
  
  public static final int AMBA_CAMERA_TIMEOUT = 8000;
  
  public static final int AMBA_COMMAND_OK = 0;
  
  public static final int AMBA_MSGID_ADJUST_CAMERA_GYRO = 6165;
  
  public static final int AMBA_MSGID_BEEPER_VOLUME = 6145;
  
  public static final int AMBA_MSGID_CAMERA_OVERHEATED = 6158;
  
  public static final int AMBA_MSGID_CANCEL_FILE_TRANSFER = 1287;
  
  public static final int AMBA_MSGID_CD = 1283;
  
  public static final int AMBA_MSGID_CLOSE_CAMERA = 6155;
  
  public static final int AMBA_MSGID_DELETE_FILE = 1281;
  
  public static final int AMBA_MSGID_DELETE_FILES_DONE = 6162;
  
  public static final int AMBA_MSGID_FILE_TRANSFER_RESULT = 7;
  
  public static final int AMBA_MSGID_FORMAT_SD = 4;
  
  public static final int AMBA_MSGID_GET_BATTERY_VOLUME = 4361;
  
  public static final int AMBA_MSGID_GET_CAMERA_ALL_MODE_PARAM = 4365;
  
  public static final int AMBA_MSGID_GET_CAMERA_ALL_PARAM = 4363;
  
  public static final int AMBA_MSGID_GET_CAMERA_ALL_SETTING_PARAM = 4364;
  
  public static final int AMBA_MSGID_GET_CAMERA_STATE = 4362;
  
  public static final int AMBA_MSGID_GET_DEVICE_NAME = 4099;
  
  public static final int AMBA_MSGID_GET_DEVICE_VERSION = 4100;
  
  public static final int AMBA_MSGID_GET_FILE = 1285;
  
  public static final int AMBA_MSGID_GET_MEDIA_INFO = 1026;
  
  public static final int AMBA_MSGID_GET_PHOTO_CAPACITY = 4357;
  
  public static final int AMBA_MSGID_GET_RECORD_TIME = 515;
  
  public static final int AMBA_MSGID_GET_SN = 4097;
  
  public static final int AMBA_MSGID_GET_STORAGE_ALL_STATE = 4358;
  
  public static final int AMBA_MSGID_GET_STORAGE_TOTAL_FREE = 5;
  
  public static final int AMBA_MSGID_GET_THUMB = 1025;
  
  public static final int AMBA_MSGID_GET_VENDOR = 4098;
  
  public static final int AMBA_MSGID_GET_VIDEO_CAPACITY = 4356;
  
  public static final int AMBA_MSGID_GET_WIFI_SETTING = 1539;
  
  public static final int AMBA_MSGID_GET_WIFI_STATUS = 1542;
  
  public static final int AMBA_MSGID_IS_SDCARD_FULL = 4354;
  
  public static final int AMBA_MSGID_IS_SDCARD_MOUNTED = 4353;
  
  public static final int AMBA_MSGID_IS_SDCARD_NEED_FORMAT = 4360;
  
  public static final int AMBA_MSGID_LS = 1282;
  
  public static final int AMBA_MSGID_MP4_FILE_SPLIT_DONE = 8195;
  
  public static final int AMBA_MSGID_PUT_FILE = 1286;
  
  public static final int AMBA_MSGID_QUERY_SESSION_HOLDER = 1793;
  
  public static final int AMBA_MSGID_RECOVERY_MEDIA_FILE = 6161;
  
  public static final int AMBA_MSGID_RESET_DEFAULT_PARAMS = 6153;
  
  public static final int AMBA_MSGID_RESET_DEFAULT_SETTINGS = 6152;
  
  public static final int AMBA_MSGID_RESET_VF = 259;
  
  public static final int AMBA_MSGID_RTC_SYNC = 6147;
  
  public static final int AMBA_MSGID_SAVE_PHOTO_DONE = 8193;
  
  public static final int AMBA_MSGID_SAVE_VIDEO_DONE = 8192;
  
  public static final int AMBA_MSGID_SDCARD_SLOWLY_WRITE = 4359;
  
  public static final int AMBA_MSGID_SET_AUTO_SHUTDOWN_TIME = 6151;
  
  public static final int AMBA_MSGID_SET_CAMERA_MODE = 4611;
  
  public static final int AMBA_MSGID_SET_CLNT_INFO = 261;
  
  public static final int AMBA_MSGID_SET_GPS_INFO = 4917;
  
  public static final int AMBA_MSGID_SET_ISO = 5126;
  
  public static final int AMBA_MSGID_SET_PHOTO_CONTINUOUS_PARAM = 4915;
  
  public static final int AMBA_MSGID_SET_PHOTO_INTERVAL_PARAM = 4929;
  
  public static final int AMBA_MSGID_SET_PHOTO_MODE = 2307;
  
  public static final int AMBA_MSGID_SET_PHOTO_SURROUNDEXP_PARAM = 4918;
  
  public static final int AMBA_MSGID_SET_PHOTO_TIMING_PARAM = 4866;
  
  public static final int AMBA_MSGID_SET_SHUTTER = 5127;
  
  public static final int AMBA_MSGID_SET_VIDEO_DELAY_PARAM = 4621;
  
  public static final int AMBA_MSGID_SET_VIDEO_FILTER = 4609;
  
  public static final int AMBA_MSGID_SET_VIDEO_MICRO_PARAM = 4618;
  
  public static final int AMBA_MSGID_SET_VIDEO_MODE = 2308;
  
  public static final int AMBA_MSGID_SET_VIDEO_SLOW_PARAM = 4916;
  
  public static final int AMBA_MSGID_SET_WHITEBALANCE = 5121;
  
  public static final int AMBA_MSGID_SET_WIFI_NEW = 5634;
  
  public static final int AMBA_MSGID_SET_WIFI_SETTING = 1538;
  
  public static final int AMBA_MSGID_SHOOT_PHOTO_CONTINUOUS = 5027;
  
  public static final int AMBA_MSGID_SHOOT_PHOTO_INTERVAL = 5025;
  
  public static final int AMBA_MSGID_SHOOT_PHOTO_NORMAL = 4864;
  
  public static final int AMBA_MSGID_SHOOT_PHOTO_TIMING = 4868;
  
  public static final int AMBA_MSGID_START_LOOP_FAIL = 4626;
  
  public static final int AMBA_MSGID_START_PHOTO_SURROUNDEXP = 5029;
  
  public static final int AMBA_MSGID_START_SESSION = 257;
  
  public static final int AMBA_MSGID_START_VIDEO_DELAY = 4623;
  
  public static final int AMBA_MSGID_START_VIDEO_MICRO = 4620;
  
  public static final int AMBA_MSGID_START_VIDEO_NORMAL = 513;
  
  public static final int AMBA_MSGID_START_VIDEO_SLOW = 5028;
  
  public static final int AMBA_MSGID_STOP_SESSION = 258;
  
  public static final int AMBA_MSGID_STOP_VF = 260;
  
  public static final int AMBA_MSGID_STOP_VIDEO = 514;
  
  public static final int AMBA_MSGID_TAKE_PHOTO = 769;
  
  public static final int AMBA_MSGID_UPDATE_HARDWARE = 8;
  
  public static final int AMBA_MSGID_WAKEUP_CAMERA = 6154;
  
  public static final int AMBA_MSGID_WIFI_RESTART = 1537;
  
  public static final int AMBA_MSGID_WIFI_START = 1541;
  
  public static final int AMBA_MSGID_WIFI_STOP = 1540;
  
  public static final int AMBA_NOTIFICATION_OK = 128;
  
  public static final int AMBA_PARAM_BATTERY_CHARGE_FULL = 68;
  
  public static final int AMBA_PARAM_BATTERY_PERCENT100 = 4;
  
  public static final int AMBA_PARAM_BATTERY_PERCENT25 = 1;
  
  public static final int AMBA_PARAM_BATTERY_PERCENT5 = 0;
  
  public static final int AMBA_PARAM_BATTERY_PERCENT50 = 2;
  
  public static final int AMBA_PARAM_BATTERY_PERCENT75 = 3;
  
  public static final int AMBA_PARAM_BEEPER_OFF = 0;
  
  public static final int AMBA_PARAM_BEEPER_VOLUME_100P = 4;
  
  public static final int AMBA_PARAM_BEEPER_VOLUME_25P = 1;
  
  public static final int AMBA_PARAM_BEEPER_VOLUME_50P = 2;
  
  public static final int AMBA_PARAM_BEEPER_VOLUME_75P = 3;
  
  public static final int AMBA_PARAM_CAMERA_MODE_PHOTO = 1;
  
  public static final int AMBA_PARAM_CAMERA_MODE_VIDEO = 0;
  
  public static final int AMBA_PARAM_CAMERA_STATE_CAPTURING = 3;
  
  public static final int AMBA_PARAM_CAMERA_STATE_CAPTURING_CONTINUOUS = 14;
  
  public static final int AMBA_PARAM_CAMERA_STATE_CAPTURING_DELAYED = 5;
  
  public static final int AMBA_PARAM_CAMERA_STATE_CAPTURING_MICRO = 4;
  
  public static final int AMBA_PARAM_CAMERA_STATE_CAPTURING_SLOW = 13;
  
  public static final int AMBA_PARAM_CAMERA_STATE_CAPTURING_SURROUNDEXP = 15;
  
  public static final int AMBA_PARAM_CAMERA_STATE_IDLE = 0;
  
  public static final int AMBA_PARAM_CAMERA_STATE_PHOTOING = 6;
  
  public static final int AMBA_PARAM_CAMERA_STATE_PHOTOING_DELAYED = 7;
  
  public static final int AMBA_PARAM_CAMERA_STATE_PHOTOING_INTERVAL = 12;
  
  public static final int AMBA_PARAM_CAMERA_STATE_STANDBY = 1;
  
  public static final int AMBA_PARAM_CAMERA_STATE_STORAGE = 2;
  
  public static final int AMBA_PARAM_SDCARD_FULL_ALMOST = 1;
  
  public static final int AMBA_PARAM_SDCARD_FULL_NO = 0;
  
  public static final int AMBA_PARAM_SDCARD_FULL_UNKNOWN = 3;
  
  public static final int AMBA_PARAM_SDCARD_FULL_YES = 2;
  
  public static final int AMBA_PARAM_SDCARD_MOUNTED_NO = 0;
  
  public static final int AMBA_PARAM_SDCARD_MOUNTED_YES = 1;
  
  public static final int AMBA_PARAM_SDCARD_NEED_FORMAT_NO = 0;
  
  public static final int AMBA_PARAM_SDCARD_NEED_FORMAT_YES = 1;
  
  public static final int AMBA_PARAM_WHITEBALANCE_AUTO = 0;
  
  public static final int AMBA_PARAM_WHITEBALANCE_CLOUDY = 3;
  
  public static final int AMBA_PARAM_WHITEBALANCE_NIGHT = 4;
  
  public static final int AMBA_PARAM_WHITEBALANCE_OUTDOOR = 1;
  
  public static final int AMBA_PARAM_WHITEBALANCE_SHADOW = 2;
  
  public static final int AMBA_RVAL_ERROR_BUSY = -21;
  
  public static final int AMBA_RVAL_ERROR_INVALID_FILE_PATH = -26;
  
  public static final int AMBA_RVAL_ERROR_INVALID_OPERATION = -14;
  
  public static final int AMBA_RVAL_ERROR_INVALID_TOKEN = -4;
  
  public static final int AMBA_RVAL_ERROR_LOW_BATTERY = -58;
  
  public static final int AMBA_RVAL_ERROR_NO_FIRMWARE = -57;
  
  public static final int AMBA_RVAL_ERROR_NO_SDCARD = -50;
  
  public static final int AMBA_RVAL_ERROR_SDCARD = -59;
  
  public static final int AMBA_RVAL_ERROR_SDCARD_FULL = -17;
  
  public static final int AMBA_RVAL_ERROR_SDCARD_SLOWLY = -61;
  
  public static final int AMBA_RVAL_ERROR_WRONG_MODE = -56;
  
  public static final int AMBA_RVAL_START_SESSION_DENIED = -3;
  
  public static int AMBA_SESSION_TOKEN = 0;
  
  public static final int AMBA_SESSION_TOKEN_INIT = 0;
  
  public static final String AMBA_SESSION_TYPE = "TCP";
  
  public static final int AMBA_UPLOAD_FILE_TYPE_FW = 1;
  
  public static final int AMBA_UPLOAD_FILE_TYPE_RW = 2;
  
  public static final String APPUBLIC_TAG = "AP_PUBLIC=";
  
  public static final String BACKSLASH_PATH_HEAD_TAG = "C:";
  
  public static final String GET_FILE_COMPLETE_TYPE = "get_file_complete";
  
  public static final String GET_FILE_FAILED_TYPE = "get_file_fail";
  
  public static final String GET_STORAGE_TYPE_FREE = "free";
  
  public static final String GET_STORAGE_TYPE_TOTAL = "total";
  
  public static final String GET_THUMB_TYPE_FULLVIEW = "fullview";
  
  public static final String GET_THUMB_TYPE_IDR = "idr";
  
  public static final String GET_THUMB_TYPE_THUMB = "thumb";
  
  public static final String PASSWORD_TAG = "AP_PASSWD=";
  
  public static final String PUT_FILE_COMPLETE_TYPE = "put_file_complete";
  
  public static final String PUT_FILE_FAIL_TYPE = "put_file_fail";
  
  public static final String SLASH_PATH_HEAD_TAG = "/tmp/SD0";
  
  public static final String SSID_TAG = "AP_SSID=";
}
