package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;

public class AMBASyncStorageAllStateResponse extends AMBAResponse {
  @Expose
  private int remain_jpg;
  
  @Expose
  private int remain_video;
  
  @Expose
  private int sd_free;
  
  @Expose
  private int sd_full;
  
  @Expose
  private int sd_mid;
  
  @Expose
  private int sd_oid;
  
  @Expose
  private String sd_pnm;
  
  @Expose
  private int sd_total;
  
  public int getRemain_jpg() {
    return this.remain_jpg;
  }
  
  public int getRemain_video() {
    return this.remain_video;
  }
  
  public int getSd_free() {
    return this.sd_free;
  }
  
  public int getSd_full() {
    return this.sd_full;
  }
  
  public int getSd_mid() {
    return this.sd_mid;
  }
  
  public int getSd_oid() {
    return this.sd_oid;
  }
  
  public String getSd_pnm() {
    return this.sd_pnm;
  }
  
  public int getSd_total() {
    return this.sd_total;
  }
  
  public void setRemain_jpg(int paramInt) {
    this.remain_jpg = paramInt;
  }
  
  public void setRemain_video(int paramInt) {
    this.remain_video = paramInt;
  }
  
  public void setSd_free(int paramInt) {
    this.sd_free = paramInt;
  }
  
  public void setSd_full(int paramInt) {
    this.sd_full = paramInt;
  }
  
  public void setSd_mid(int paramInt) {
    this.sd_mid = paramInt;
  }
  
  public void setSd_oid(int paramInt) {
    this.sd_oid = paramInt;
  }
  
  public void setSd_pnm(String paramString) {
    this.sd_pnm = paramString;
  }
  
  public void setSd_total(int paramInt) {
    this.sd_total = paramInt;
  }
}
