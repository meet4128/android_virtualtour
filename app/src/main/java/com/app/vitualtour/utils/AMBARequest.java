package com.app.vitualtour.utils;

import com.google.gson.annotations.Expose;
import java.io.Serializable;

public class AMBARequest implements Serializable {
  public static final int ERROR_EXCEPTION = 10001;
  
  public static final int ERROR_TIMEOUT = 10000;
  
  @Expose
  private int msg_id;
  
  @Expose
  private String param;
  
  private Class<? extends AMBAResponse> responseClass;
  
  private ResponseListener responseListener;
  
  public boolean shouldWaitUntilPreviousResponded = true;
  
  private long timestamp;
  
  @Expose
  private int token;
  
  @Expose
  private String type;
  
  public AMBARequest(ResponseListener paramResponseListener) {
    this(paramResponseListener, AMBAResponse.class);
  }
  
  public AMBARequest(ResponseListener paramResponseListener, Class<? extends AMBAResponse> paramClass) {
    this.responseListener = paramResponseListener;
    this.responseClass = paramClass;
  }
  
  public int getMsg_id() {
    return this.msg_id;
  }
  
  public String getParam() {
    return this.param;
  }
  
  public String getRequestKey() {
    return Integer.toString(this.msg_id);
  }
  
  public Class<? extends AMBAResponse> getResponseClass() {
    return this.responseClass;
  }
  
  public ResponseListener getResponseListener() {
    return this.responseListener;
  }
  
  public long getTimestamp() {
    return this.timestamp;
  }
  
  public int getToken() {
    return this.token;
  }
  
  public String getType() {
    return this.type;
  }
  
  public void setMsg_id(int paramInt) {
    this.msg_id = paramInt;
  }
  
  public void setParam(String paramString) {
    this.param = paramString;
  }
  
  public void setResponseListener(ResponseListener paramResponseListener) {
    this.responseListener = paramResponseListener;
  }
  
  public void setTimestamp(long paramLong) {
    this.timestamp = paramLong;
  }
  
  public void setToken(int paramInt) {
    this.token = paramInt;
  }
  
  public void setType(String paramString) {
    this.type = paramString;
  }
  
  public String toString() {
    return "AMBARequest(" + hashCode() + ") : msgID=" + this.msg_id + ", token=" + this.token + ", param=" + this.param;
  }
  
  public static interface ResponseListener {
    void onResponseError(AMBARequest param1AMBARequest, int param1Int, String param1String);
    
    void onResponseReceived(AMBAResponse param1AMBAResponse);
  }
}
