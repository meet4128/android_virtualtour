package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;

public class AMBASetClientInfoResponse extends AMBAResponse {
  @Expose
  private int update;
  
  public int getUpdate() {
    return this.update;
  }
  
  public void setUpdate(int paramInt) {
    this.update = paramInt;
  }
}


/* Location:              /Users/makavanadhaiyur/Documents/Dhaiyur/dex2jar/classes10-dex2jar.jar!/com/madv360/madv/connection/ambaresponse/AMBASetClientInfoResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */