package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;
import java.util.List;
import java.util.Map;

public class AMBAListResponse extends AMBAResponse {
  @Expose
  public List<Map<String, String>> listing;
}


/* Location:              /Users/makavanadhaiyur/Documents/Dhaiyur/dex2jar/classes10-dex2jar.jar!/com/madv360/madv/connection/ambaresponse/AMBAListResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */