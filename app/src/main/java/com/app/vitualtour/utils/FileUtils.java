package com.app.vitualtour.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class FileUtils {

    private final static String ROOT_DIRECTORY = "/DSMTracker/";
    private final static String ASSESSMENT_DIRECTORY = "/DSMTracker/";

    public final static String ACTIVE_GROUPS_FILE = "testr.json";

    private static File getRootDirectory(Context context) {
        return new File(context.getCacheDir() + ROOT_DIRECTORY);
    }

    private static void createAllDirectory(Context context) {

        File rootDir = getRootDirectory(context);
        if (!rootDir.exists())
            rootDir.mkdir();

        File subDir = new File(rootDir + "/" + ASSESSMENT_DIRECTORY);
        if (!rootDir.exists())
            rootDir.mkdir();
    }

    public static boolean isFileExists(Context context, String fileName) {

        File rootDir = getRootDirectory(context);

        if (!rootDir.exists())
            rootDir.mkdirs();

        return new File(rootDir, fileName).exists();
    }

    public static boolean writeInfoToFile(Context context, String fileName, String data) {

        try {

            File rootDir = getRootDirectory(context);

            if (!rootDir.exists())
                rootDir.mkdirs();

            File file = new File(rootDir, fileName);

            if (file.exists())
                file.delete();

            file.createNewFile();

            FileOutputStream fOut = new FileOutputStream(file);

            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);
            myOutWriter.close();
            fOut.close();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String readInfoFromFile(Context context, String fileName) {

        try {

            File rootDir = getRootDirectory(context);

            File file = new File(rootDir, fileName);

            FileInputStream fIn = new FileInputStream(file);

            BufferedReader myReader = new BufferedReader(new InputStreamReader(fIn));

            String aDataRow = "";
            String aBuffer = "";

            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }

            myReader.close();

            return aBuffer;

        } catch (Exception e) {
            return "";
        }
    }

    public static void copyFile(String inputFile, String outputFile) {

        try {

            InputStream in = new FileInputStream(inputFile);
            OutputStream out = new FileOutputStream(outputFile);

            byte[] buffer = new byte[1024];
            int read;

            while ((read = in.read(buffer)) != -1)
                out.write(buffer, 0, read);

            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("AppUtils.Java", e.getMessage());
        }
    }

    public static boolean deleteFile(String filepath) {

        File file = new File(filepath);

        try {
            return file.exists() && file.delete();
        } catch (Exception e) {
            return false;
        }
    }

    public static String getOutputMediaFile(int type) {

        final int TAKE_PICTURE = 1, TAKE_VIDEO = 2;

        final String MEDIA_DIRECTORY = "InZoneUser";

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), MEDIA_DIRECTORY);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(MEDIA_DIRECTORY, "Oops! Failed create " + MEDIA_DIRECTORY + " directory");
                return null;
            }
        }

        if (type == TAKE_PICTURE) {
            return mediaStorageDir.getPath() + File.separator + "IMG_" + System.currentTimeMillis() + ".jpg";
        } else if (type == TAKE_VIDEO) {
            return mediaStorageDir.getPath() + File.separator + "VID_" + System.currentTimeMillis() + ".mp4";
        } else {
            return null;
        }
    }
    public static String readAssetJSONFile(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }
    public static String readAssetJSONFileBGEDR(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/BGEDR/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static String readAssetJSONFileEScore(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/eScore/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static String readAssetJSONFileSmallBusiness(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/smallbusiness/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static String readAssetJSONFileWholeHome(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/wholeHome/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static String readAssetJSONFileAPCOPH2(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/apco/" + filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }




    public static String readAssetJSONFileSmallBusinessAPCO(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/smallbusiness/apco/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static String readAssetJSONFileSiteVisit(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/retail/sitevisit/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static String readAssetJSONFileConsumerEvent(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/retail/consumerEvent/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static String readAssetJSONFileQAQC(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/retail/qa_qc/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static String readAssetJSONFileSafetyRideAlong(Context context, String filename) throws IOException {
        InputStream file = context.getAssets().open("json/retail/safetyRide/"+filename + ".json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }


    public static String copyToCache(Context context, File sourceFilePath) {
        try {
            if (!sourceFilePath.exists()) {
                return "";
            }

            //String filename = sourceFilePath.getName();
            String filename = String.valueOf(System.currentTimeMillis()) + ".png";

            File destFile = new File(context.getCacheDir(), "Photo");

            if (!destFile.exists()) {
                destFile.mkdir();
            }

            File file = new File(destFile, filename);

            FileUtils.copyFile(sourceFilePath.getAbsolutePath(), file.getAbsolutePath());

            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String saveToSdCard(Context context,Bitmap bitmap, String filename) {

        String stored = null;

        File folder = new File(context.getCacheDir(), "Photo");
        folder.mkdir();
        File file = new File(folder.getAbsoluteFile(), filename) ;
        if (file.exists())
            return stored ;

        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            stored = "success";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.toString();
    }

    public static File getImage(String imagename) {

        File mediaImage = null;
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root);
            if (!myDir.exists())
                return null;

            mediaImage = new File(myDir.getPath() + "/.your_specific_directory/"+imagename);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mediaImage;
    }
    public static boolean checkifImageExists(String imagename)
    {
        Bitmap b = null ;
        File file = getImage("/"+imagename+".png");
        String path = file.getAbsolutePath();

        if (path != null)
            b = BitmapFactory.decodeFile(path);

        if(b == null ||  b.equals(""))
        {
            return false ;
        }
        return true ;
    }
}