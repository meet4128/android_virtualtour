package com.app.vitualtour.utils;

import com.google.gson.Gson;

public class SendData
{
    int msg_id;
    int token;

    public int getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(int msg_id) {
        this.msg_id = msg_id;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public String objectToJson(SendData bean)
    {
        Gson gson = new Gson();
        String json = gson.toJson(bean);
        return json;
    }

    //{"msg_id":256,"token":1}
}
