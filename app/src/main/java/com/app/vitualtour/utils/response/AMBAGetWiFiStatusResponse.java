package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;

import java.util.Map;

public class AMBAGetWiFiStatusResponse extends AMBAResponse {
  public String MAC() {
    return (String)((Map)this.param).get("MAC");
  }
  
  public String SSID() {
    return (String)((Map)this.param).get("SSID");
  }
}


/* Location:              /Users/makavanadhaiyur/Documents/Dhaiyur/dex2jar/classes10-dex2jar.jar!/com/madv360/madv/connection/ambaresponse/AMBAGetWiFiStatusResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */