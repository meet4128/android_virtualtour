package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;

public class AMBAGetFileResponse extends AMBAResponse {
  @Expose
  private long rem_size;
  
  @Expose
  private int size;
  
  public long getRemSize() {
    return (this.rem_size < 0L) ? (4294967296L + this.rem_size) : this.rem_size;
  }
  
  public long getSize() {
    return (this.size < 0) ? (4294967296L + this.size) : this.size;
  }
}
