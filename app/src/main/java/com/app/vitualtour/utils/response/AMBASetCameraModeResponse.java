package com.app.vitualtour.utils.response;

import com.app.vitualtour.utils.AMBAResponse;
import com.google.gson.annotations.Expose;


public class AMBASetCameraModeResponse extends AMBAResponse {
  @Expose
  private int sub_mode;
  
  @Expose
  private int sub_mode_param;
  
  public int getSub_mode() {
    return this.sub_mode;
  }
  
  public int getSub_mode_param() {
    return this.sub_mode_param;
  }
  
  public void setSub_mode(int paramInt) {
    this.sub_mode = paramInt;
  }
  
  public void setSub_mode_param(int paramInt) {
    this.sub_mode_param = paramInt;
  }
}


/* Location:              /Users/makavanadhaiyur/Documents/Dhaiyur/dex2jar/classes10-dex2jar.jar!/com/madv360/madv/connection/ambaresponse/AMBASetCameraModeResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       1.1.3
 */