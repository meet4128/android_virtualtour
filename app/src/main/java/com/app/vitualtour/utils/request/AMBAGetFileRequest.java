package com.app.vitualtour.utils.request;

import com.app.vitualtour.utils.AMBARequest;
import com.app.vitualtour.utils.response.AMBAGetFileResponse;
import com.google.gson.annotations.Expose;


public class AMBAGetFileRequest extends AMBARequest {
  @Expose
  public long fetch_size;
  
  @Expose
  public String offset;
  
  public AMBAGetFileRequest(AMBARequest.ResponseListener paramResponseListener) {
    super(paramResponseListener, AMBAGetFileResponse.class);
  }
}
