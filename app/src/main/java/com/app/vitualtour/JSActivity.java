package com.app.vitualtour;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.vitualtour.activity.sitemap.SiteMapActivity;
import com.app.vitualtour.model.htmldata.Default;
import com.app.vitualtour.model.htmldata.HotSpots;
import com.app.vitualtour.model.htmldata.Scenes;
import com.app.vitualtour.model.htmldata.ScreenName;
import com.app.vitualtour.realm.PropertyBean;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.AgentWebSettingsImpl;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.IAgentWebSettings;
import com.just.agentweb.WebChromeClient;
import com.just.agentweb.WebViewClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Logger;

public class JSActivity extends AppCompatActivity {
    WebView mWebview;
    protected AgentWeb mAgentWeb;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_js);
        mLinearLayout = findViewById(R.id.web_content);
        String images = getIntent().getStringExtra("images");
        readAssets(images);
    }

    private WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            Log.d("progress", "" + newProgress);
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);

        }
    };

    private WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.i("Info", "BaseWebActivity onPageStarted");
        }
    };

    public @Nullable
    IAgentWebSettings getAgentWebSettings() {
        return AgentWebSettingsImpl.getInstance();
    }

    @Override
    protected void onPause() {
        mAgentWeb.getWebLifeCycle().onPause();
        super.onPause();

    }

    @Override
    protected void onResume() {
        mAgentWeb.getWebLifeCycle().onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAgentWeb.getWebLifeCycle().onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 2) {
            mAgentWeb.getIEventHandler().back();

        }
    }

    private void readAssets(String imagePath) {
        try {
            InputStream is = getAssets().open("a.html");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            Bitmap bm = BitmapFactory.decodeFile(imagePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); // bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();
            String encodedImage =  Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
            //String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            String str = new String(buffer);
            str = str.replace("image1","data:image/jpeg;base64,"+encodedImage ).
                    replace("image2","data:image/jpeg;base64,"+encodedImage);
            Log.d("Data info",str);

            Default defaultInfo=new Default();
            defaultInfo.setFirstScene("Circle");
            defaultInfo.setAutoLoad(true);
            defaultInfo.setSceneFadeDuration(1000);

            ArrayList<HotSpots>hotSpotsArrayList=new ArrayList<>();
            HotSpots hotSpots=new HotSpots();
            hotSpots.setPitch();
            hotSpots.setYaw();
            hotSpots.setType();
            hotSpots.setText();
            hotSpots.setSceneId();
            hotSpots.setTargetYaw();
            hotSpots.setTargetPitch();
            hotSpotsArrayList.add(hotSpots);

            ScreenName screenName=new ScreenName();
            screenName.setHfov();
            screenName.setYaw();
            screenName.setHotSpots();
            screenName.setPanorama();
            screenName.setPitch();
            screenName.setTitle();
            screenName.setType();
            saveHtmlFile(str);
        } catch (Exception ioException) {
            ioException.printStackTrace();
        }

    }

    private void saveHtmlFile(String htmlContent) {

        String path = getExternalFilesDir(null).getAbsolutePath();
        String fileName = DateFormat.format("dd_MM_yyyy_hh_mm_ss", System.currentTimeMillis()).toString();
        fileName = fileName + ".html";
        File file = new File(path, fileName);

        try {
            FileOutputStream out = new FileOutputStream(file);
            byte[] data = htmlContent.getBytes();
            out.write(data);
            out.close();
            Log.e("Data", "File Save : " + file.getPath());

            mAgentWeb = AgentWeb.with(this)
                    .setAgentWebParent(mLinearLayout, new LinearLayout.LayoutParams(-1, -1))
                    .closeIndicator()
                    .setWebChromeClient(mWebChromeClient)
                    .setWebViewClient(mWebViewClient)
                    .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
                    .setAgentWebWebSettings(getAgentWebSettings())
                    .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                    .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.ASK)
                    //.interceptUnkownUrl()
                    .createAgentWeb()
                    .ready()
                    .go("file://"+file.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
