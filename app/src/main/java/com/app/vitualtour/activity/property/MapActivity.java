package com.app.vitualtour.activity.property;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.vitualtour.R;
import com.app.vitualtour.realm.PropertyBean;
import com.app.vitualtour.realm.RealmController;
import com.appolica.interactiveinfowindow.InfoWindow;
import com.appolica.interactiveinfowindow.InfoWindowManager;
import com.appolica.interactiveinfowindow.fragment.MapInfoWindowFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, InfoWindowManager.WindowShowListener,
		GoogleMap.OnMarkerClickListener
{

	GoogleMap mGoogleMap;
	List<PropertyBean> mListProperty;
	private TextView mBackTv;
	private ImageView mCreatePropertyIv;
	private InfoWindow formWindow;
	private InfoWindowManager infoWindowManager;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);


		mListProperty = RealmController.getInstance().getAllProperties();

		MapInfoWindowFragment mapInfoWindowFragment =
				(MapInfoWindowFragment) getSupportFragmentManager().findFragmentById(R.id.infoWindowMap);
		mapInfoWindowFragment.getMapAsync(this);

		infoWindowManager = mapInfoWindowFragment.infoWindowManager();
		infoWindowManager.setHideOnFling(true);
		infoWindowManager.setWindowShowListener(MapActivity.this);

		mBackTv=findViewById(R.id.back_tv);
		mBackTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});


		mCreatePropertyIv=findViewById(R.id.add_property_iv);
		mCreatePropertyIv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(MapActivity.this,CreatePropertyActivity.class);
				startActivity(i);
				finish();
			}
		});

	}


	@Override
	public void onMapReady(GoogleMap googleMap) {
			mGoogleMap = googleMap;
		LatLng sydney = null;
		for (int i = 0; i < mListProperty.size(); i++) {
			createMarker(googleMap, mListProperty.get(i).getMlatitude(), mListProperty.get(i).getMlongitude(), mListProperty.get(i).getmPropertyId());

			if(sydney==null)
			{
				sydney = new LatLng(mListProperty.get(i).getMlatitude(), mListProperty.get(i).getMlongitude());
			}
		}
		if(sydney!=null)
		{
			mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(10));
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
			mGoogleMap.setOnMarkerClickListener(MapActivity.this);
		}
	}

	protected Marker createMarker(GoogleMap googleMap, double latitude, double longitude, String id) {

		return googleMap.addMarker(new MarkerOptions()
				.position(new LatLng(latitude, longitude))
				.snippet(id)
				.title(""));
	}

	@Override
	public void onWindowShowStarted(@NonNull InfoWindow infoWindow) {

	}

	@Override
	public void onWindowShown(@NonNull InfoWindow infoWindow) {

	}

	@Override
	public void onWindowHideStarted(@NonNull InfoWindow infoWindow) {

	}

	@Override
	public void onWindowHidden(@NonNull InfoWindow infoWindow) {

	}

	@Override
	public boolean onMarkerClick(Marker marker) {

		final int offsetX = (int) getResources().getDimension(R.dimen.marker_offset_x);
		final int offsetY = (int) getResources().getDimension(R.dimen.marker_offset_y);

		final InfoWindow.MarkerSpecification markerSpec =
				new InfoWindow.MarkerSpecification(offsetX, offsetY);
		InfoWindow infoWindow = null;
		infoWindow = new InfoWindow(marker, markerSpec, FormFragment.newInstance(marker.getSnippet()));


		if (infoWindow != null) {
			infoWindowManager.toggle(infoWindow, true);
		}
		return true;
	}
}
