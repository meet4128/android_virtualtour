package com.app.vitualtour.activity.orderslink;

import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.vitualtour.R;
import com.app.vitualtour.adapter.CustomAdapter;
import com.app.vitualtour.realm.FloorBean;
import com.app.vitualtour.realm.PointsBean;
import com.app.vitualtour.realm.PropertyBean;
import com.app.vitualtour.realm.RealmController;
import com.app.vitualtour.view.DefaltCircle;
import com.app.vitualtour.view.PinViewLines;
import com.davemorrissey.labs.subscaleview.ImageSource;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class SetLinesActivity extends AppCompatActivity {
	Spinner mFloorSp;
	TextView mBackTv;
	CustomAdapter mSpinnerAdapter;
	List<FloorBean> mlistFloor = new ArrayList<>();
	FloorBean mCurrentBean;
	PropertyBean mBean;
	PinViewLines mTouchableIv;
	private RealmList<PointsBean> mListPoint;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lines_orders);

		mBean = RealmController.getInstance().getProperty(getIntent().getExtras().getString("id"));

		mBackTv = findViewById(R.id.back_tv);
		mBackTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		mFloorSp = findViewById(R.id.floor_sp);

		mlistFloor = RealmController.getInstance().getFloorByProp(mBean.getmPropertyId());
		mSpinnerAdapter = new CustomAdapter(this, mlistFloor);
		mFloorSp.setAdapter(mSpinnerAdapter);

		mTouchableIv = findViewById(R.id.touchble_image);

		mFloorSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				mCurrentBean = mlistFloor.get(i);
				mListPoint = (RealmList<PointsBean>) mCurrentBean.getmListPoint();
				mTouchableIv.setCurrentFloor(mCurrentBean);
				if (mCurrentBean != null && mCurrentBean.getmFloorImage() != null) {
					mTouchableIv.setImage(ImageSource.uri(mCurrentBean.getmFloorImage()));
					mTouchableIv.updatePoints(mCurrentBean.getmListPoint());
					mTouchableIv.updateLines(mCurrentBean.getmListLines());

				} else {
					mTouchableIv.reset();
					mTouchableIv.setImage(ImageSource.resource(R.drawable.trans));
					mTouchableIv.updatePoints(mCurrentBean.getmListPoint());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

	}
}
