package com.app.vitualtour.activity.property;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.vitualtour.R;
import com.app.vitualtour.realm.PropertyBean;
import com.app.vitualtour.realm.RealmController;
import com.bumptech.glide.Glide;

public class FormFragment extends Fragment {

    public static FormFragment newInstance(String propertyId)
    {
        FormFragment myFragment = new FormFragment();
        Bundle args = new Bundle();
        args.putString("id", propertyId);
        myFragment.setArguments(args);
        return myFragment;
    }

    PropertyBean mBean;
    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.info_window_form_fragment, container, false);

        mBean= RealmController.getInstance().getProperty(getArguments().getString("id")) ;

        return view;
    }

    TextView mNameTv;
    TextView mAddressTv;
    ImageView mImageIv;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),ViewPropertyActivity.class);
                i.putExtra("bean",mBean.getmPropertyId());
                startActivity(i);
            }
        };

        mNameTv=view.findViewById(R.id.name_tv);
        mAddressTv=view.findViewById(R.id.address_tv);
        mImageIv=view.findViewById(R.id.property_iv);


        mNameTv.setText(mBean.getmName());
        mAddressTv.setText(mBean.getmAddress());
        Glide.with(this).load(mBean.getmImage()).into(mImageIv);


        mNameTv.setOnClickListener(onClickListener);
        mAddressTv.setOnClickListener(onClickListener);
        mImageIv.setOnClickListener(onClickListener);
    }
}