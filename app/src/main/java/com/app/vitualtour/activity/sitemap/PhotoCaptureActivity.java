package com.app.vitualtour.activity.sitemap;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import com.app.vitualtour.R;
import com.app.vitualtour.utils.FileUtils;
import com.app.vitualtour.utils.SendData;
import com.chtj.socket.BaseTcpSocket;
import com.chtj.socket.ISocketListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.thanosfisherman.wifiutils.WifiUtils;

import org.json.JSONObject;
import java.net.URL;
import java.net.URLConnection;


public class PhotoCaptureActivity extends AppCompatActivity {
    String mPhotoType;
    private TextView mTitleTv;
    private TextView mInstructionTv;
    String[] instruction = {"Place the 360 camera in the desired outside location move out of sight.", "Place the 360 camera in the middle of the room move out of sight", "Place the 360 camera at the  stairs move out of sight", "Place the 360 camera in the window move out of sight", "Place the 360 camera in the door way move out of sight"};
    private static final int REQUEST_CODE_CHOOSE = 25;
    CardView mTakePhotoCV;
    CardView mSavePhotoCv;
    VrPanoramaView mPreviewIv;
    String paths;

    VrPanoramaView.Options option;
    BaseTcpSocket baseTcpSocket;
    public static final String TAG = "Greeva";

    private ProgressDialog dialog;

    int SessionId = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_capture);

        /*PhotoCaptureActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                dialog = new ProgressDialog(PhotoCaptureActivity.this);
                dialog.setMessage("Connecting to Camera..");
                dialog.show();
            }
        });*/
        
        /*baseTcpSocket = new BaseTcpSocket("192.168.42.1", 7878, 20000);
        // monitor callback
        baseTcpSocket.setSocketListener(new ISocketListener() {
            @Override
            public void recv(byte[] data, int offset, int size) {
                Log.d(TAG, "read content successful");
                try {
                    JSONObject jobje = new JSONObject(new String(data));

                    if (jobje.getInt("msg_id") == 257) {
                        SessionId = jobje.getInt("param");
                    } else if (jobje.getInt("msg_id") == 8193) {
                        Log.d("response", jobje.toString());

                        String photo[] = jobje.getString("param").split("\\\\");

                        String downloadURL = "http://192.168.42.1:50422/" + photo[2] + "/" + photo[3];

                        new GetImages(downloadURL, photo[3]).execute();
                    }
                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                }
            }

            @Override
            public void writeSuccess(byte[] data) {
                Log.d(TAG, "write content successful");
            }

            @Override
            public void connSuccess() {

                PhotoCaptureActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }
                });


                SendData connectTOCamera = new SendData();
                connectTOCamera.setMsg_id(257);
                connectTOCamera.setToken(0);
                String makeRequest = connectTOCamera.objectToJson(connectTOCamera);
                baseTcpSocket.send(makeRequest.getBytes());
            }

            @Override
            public void connFaild(Throwable t) {
                Log.d(TAG, "The connection is connFaild");
                if (dialog.isShowing()) {
                    PhotoCaptureActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            dialog.cancel();
                            Toast.makeText(PhotoCaptureActivity.this, "Please Connect device to Camera WIFI.", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }

            @Override
            public void connClose() {
                Log.d(TAG, "The connection is disconnect");
            }
        });
        baseTcpSocket.connect(this);*/


        mPhotoType = getIntent().getExtras().getString("photoType");

        mTitleTv = findViewById(R.id.title_tv);
        mTitleTv.setText(mPhotoType + " 360 Capture");

        mInstructionTv = findViewById(R.id.instruction_tv);

        option = new VrPanoramaView.Options();
        option.inputType = VrPanoramaView.Options.TYPE_MONO;

        if (mPhotoType.equals("Outside")) {
            mInstructionTv.setText(instruction[0]);
        } else if (mPhotoType.equals("Room")) {
            mInstructionTv.setText(instruction[1]);
        } else if (mPhotoType.equals("Stairs")) {
            mInstructionTv.setText(instruction[2]);
        } else if (mPhotoType.equals("Window")) {
            mInstructionTv.setText(instruction[3]);
        } else if (mPhotoType.equals("Door")) {
            mInstructionTv.setText(instruction[4]);
        }

        mPreviewIv = findViewById(R.id.preview_iv);

        mSavePhotoCv = findViewById(R.id.add_property_card);
        mSavePhotoCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (paths != null) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result_File", paths);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }

            }
        });

        mTakePhotoCV = findViewById(R.id.take_photo_cv);
        mTakePhotoCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendData CapturePhoto = new SendData();
                CapturePhoto.setMsg_id(4864);
                CapturePhoto.setToken(SessionId);
                String makeRequest = CapturePhoto.objectToJson(CapturePhoto);
               // baseTcpSocket.send(makeRequest.getBytes());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private class GetImages extends AsyncTask<Object, Object, Object> {
        private String requestUrl, imagename_;
        private Bitmap bitmap;
        private ProgressDialog dialog1;

        private GetImages(String requestUrl, String _imagename_) {
            this.requestUrl = requestUrl;
            this.imagename_ = _imagename_;

        }

        @Override
        protected void onPreExecute() {

            PhotoCaptureActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    dialog1 = new ProgressDialog(PhotoCaptureActivity.this);
                    dialog1.setMessage("Downloading image..please wait");
                    dialog1.show();
                }
            });
        }

        @Override
        protected Object doInBackground(Object... objects) {
            try {
                URL url = new URL(requestUrl);
                URLConnection conn = url.openConnection();
                bitmap = BitmapFactory.decodeStream(conn.getInputStream());
            } catch (Exception ex) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            mPreviewIv.loadImageFromBitmap(bitmap, option);
            paths = FileUtils.saveToSdCard(PhotoCaptureActivity.this, bitmap, imagename_);
            if (dialog1.isShowing()) {
                dialog1.dismiss();
            }
        }
    }
}
