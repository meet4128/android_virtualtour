package com.app.vitualtour.activity.sitemap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.vitualtour.R;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.zhihu.matisse.Matisse;

import java.io.IOException;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

public class PhotoTypeSelectionActivity extends AppCompatActivity {

	private SegmentedGroup mSegmentControll;
	private TextView mSaveTv;

	private RadioButton mOutsideRb;
	private RadioButton mRoomRb;
	private RadioButton mStairsRb;
	private RadioButton mWindowRb;
	private RadioButton mDoorRb;
	private EditText mPhotoNameEt;
	private SegmentedGroup mSegmentGp;

	String mColor="";
	String mType="";
	String mName="";
	String mPhotoUrl="";
	VrPanoramaView mPreviewIv;
	VrPanoramaView.Options option;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_selection_type);

		mSegmentControll=findViewById(R.id.segmented2);
		mSegmentControll.setTintColor(getResources().getColor(R.color.colorPrimary));

		mSaveTv=findViewById(R.id.save_tv);

		mPhotoNameEt=findViewById(R.id.photo_name_et);

		mSaveTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{

				if(mPhotoNameEt.getText().toString().trim().isEmpty())
				{
					Toast.makeText(PhotoTypeSelectionActivity.this,"Photo Name must be entered.",Toast.LENGTH_SHORT).show();
				}else if(mPhotoUrl==null){
					Toast.makeText(PhotoTypeSelectionActivity.this,"Image must be selected.",Toast.LENGTH_SHORT).show();
				}else{
					Intent returnIntent = new Intent();
					returnIntent.putExtra("PhotoUrl",mPhotoUrl);
					returnIntent.putExtra("PhotoName",mPhotoNameEt.getText().toString());
					returnIntent.putExtra("Colors",mColor);
					returnIntent.putExtra("Type",mType);
					setResult(Activity.RESULT_OK,returnIntent);
					finish();
				}
			}
		});

		mPreviewIv=findViewById(R.id.preview_iv);
		option= new VrPanoramaView.Options();
		option.inputType = VrPanoramaView.Options.TYPE_MONO;

		mOutsideRb=findViewById(R.id.outside_rb);
		mRoomRb=findViewById(R.id.room_rb);
		mStairsRb=findViewById(R.id.stairs_rb);
		mWindowRb=findViewById(R.id.window_rb);
		mDoorRb=findViewById(R.id.door_rb);

		mSegmentGp=findViewById(R.id.segmented2);
		mSegmentGp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int i) {

				if(i==R.id.outside_rb)
				{
					mColor="#1fff09";
					mType="Outside";
				}else if(i==R.id.room_rb)
				{
					mColor="#ff0000";
					mType="Room";
				}else if(i==R.id.stairs_rb)
				{
					mColor="#ff09ee";
					mType="Stairs";
				}else if(i==R.id.window_rb)
				{
					mColor="#fff009";
					mType="Window";
				}else if(i==R.id.door_rb)
				{
					mColor="#0a09ff";
					mType="Door";
				}

				Intent intetn=new Intent(PhotoTypeSelectionActivity.this,PhotoCaptureActivity.class);
				intetn.putExtra("photoType",mType);
				startActivityForResult(intetn,100);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		 if(requestCode==100 && resultCode==RESULT_OK)
		{
			mPhotoUrl=data.getStringExtra("result_File");

			Bitmap bitmap = BitmapFactory.decodeFile(mPhotoUrl);
			mPreviewIv.loadImageFromBitmap(bitmap,option);
		}

	}
}
