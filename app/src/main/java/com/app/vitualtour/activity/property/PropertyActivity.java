package com.app.vitualtour.activity.property;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.vitualtour.R;
import com.app.vitualtour.adapter.PropertyAdapter;
import com.app.vitualtour.realm.PropertyBean;
import com.app.vitualtour.realm.RealmController;
import com.app.vitualtour.utils.CamConnection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class PropertyActivity extends AppCompatActivity implements PropertyAdapter.OnItemClickListener {

	private PropertyAdapter mPropertyAdapter;
	private RecyclerView mPropertyRv;
	List<PropertyBean> mListProperty;
	private TextView mMapTv;
	private TextView mCreatePropertyTv;

	private Realm mRealm;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_property);
		RealmController.with(this);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		mRealm = RealmController.getInstance().getRealm();


		mPropertyRv = findViewById(R.id.property_rv);
		RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
		mPropertyRv.setLayoutManager(layoutManager);



		mMapTv = findViewById(R.id.map_tv);
		mMapTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(PropertyActivity.this, MapActivity.class);
				startActivity(i);
			}
		});
		mCreatePropertyTv = findViewById(R.id.create_property_tv);
		mCreatePropertyTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(PropertyActivity.this, CreatePropertyActivity.class);
				startActivity(i);
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		mListProperty=new ArrayList<>();
		mListProperty = RealmController.getInstance().getAllProperties();
		mPropertyAdapter = new PropertyAdapter(this, mListProperty,this);
		mPropertyRv.setAdapter(mPropertyAdapter);
	}






	@Override
	public void onItemClick(String pos) {
		Intent i=new Intent(PropertyActivity.this,ViewPropertyActivity.class);
		i.putExtra("bean",pos);
		startActivity(i);
	}

	@Override
	public void onDeteleClick(final String pos) {
		new AlertDialog.Builder(this)
				.setTitle("Delete Property")
				.setMessage("Are you sure want to Delete Property?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						PropertyBean mBean= RealmController.getInstance().getProperty(pos) ;

						mRealm.beginTransaction();
						mBean.deleteFromRealm();
						mRealm.commitTransaction();

						mListProperty = RealmController.getInstance().getAllProperties();
						mPropertyAdapter = new PropertyAdapter(PropertyActivity.this, mListProperty,PropertyActivity.this);
						mPropertyRv.setAdapter(mPropertyAdapter);
					}
				})
				.setNegativeButton("No", null)
				.show();

	}
}
