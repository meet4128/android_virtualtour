package com.app.vitualtour.activity.sitemap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.app.vitualtour.JSActivity;
import com.app.vitualtour.R;
import com.app.vitualtour.activity.orderslink.AddOrderActivity;
import com.app.vitualtour.activity.orderslink.SetLinesActivity;
import com.app.vitualtour.adapter.CustomAdapter;
import com.app.vitualtour.realm.FloorBean;
import com.app.vitualtour.realm.PointsBean;
import com.app.vitualtour.realm.PropertyBean;
import com.app.vitualtour.realm.RealmController;
import com.app.vitualtour.realm.TourBean;
import com.app.vitualtour.view.GifSizeFilter;
import com.app.vitualtour.view.PinView;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;

public class SiteMapActivity extends AppCompatActivity {

	private static final int REQUEST_CODE_CHOOSE = 24;
	Spinner mFloorSp;

	private TextView mCreateTourTv;
	PinView mTouchableIv;
	CardView mAddImageCd;

	private Realm mRealm;
	PropertyBean mBean;

	private TextView mPropertyTv;
	private CardView mAddFloorCv;
	CustomAdapter mSpinnerAdapter;
	TourBean mTour;

	List<FloorBean> mlistFloor=new ArrayList<>();

	PointF sCoord;
	FloorBean mCurrentBean;

	CardView mAddOrderCv;
	CardView mLinkPhotoCv;
	CardView mVirtualTourCv;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sitemap);

		mFloorSp=findViewById(R.id.floor_sp);

		mRealm=RealmController.getInstance().getRealm();
		mBean= RealmController.getInstance().getProperty(getIntent().getExtras().getString("id")) ;

		mAddFloorCv=findViewById(R.id.add_floor_cv);
		mAddFloorCv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showDialog(SiteMapActivity.this,"");
			}
		});

		mLinkPhotoCv=findViewById(R.id.link_cv);
		mLinkPhotoCv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(SiteMapActivity.this, SetLinesActivity.class);
				i.putExtra("id",mBean.getmPropertyId());
				startActivity(i);
			}
		});

		mVirtualTourCv=findViewById(R.id.virtual_tour_cv);
		mVirtualTourCv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(SiteMapActivity.this, JSActivity.class);
				Bundle b = new Bundle();
				b.putString("images",mBean.getmImage());
				i.putExtras(b);
				startActivity(i);
			}
		});

		mTour=RealmController.getInstance().getTour(mBean.getmPropertyId());

		if(mTour==null)
		{
			mTour=new TourBean();
			mTour.setmTourId("Tour_"+UUID.randomUUID());
			mTour.setmPropertyId(mBean.getmPropertyId());

			mRealm.beginTransaction();
			mRealm.insertOrUpdate(mTour);
			mRealm.commitTransaction();
		}

		mAddOrderCv=findViewById(R.id.add_order_cv);
		mAddOrderCv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i=new Intent(SiteMapActivity.this, AddOrderActivity.class);
				i.putExtra("id",mBean.getmPropertyId());
				startActivity(i);
			}
		});


		mPropertyTv=findViewById(R.id.prop_name_tv);
		mPropertyTv.setText(mBean.getmName());

		mlistFloor=RealmController.getInstance().getFloorByProp(mBean.getmPropertyId());

		mSpinnerAdapter=new CustomAdapter(this,mlistFloor);
		mFloorSp.setAdapter(mSpinnerAdapter);


		mFloorSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				mCurrentBean=mlistFloor.get(i);
				if(mCurrentBean!=null && mCurrentBean.getmFloorImage()!=null)
				{
					mTouchableIv.setImage(ImageSource.uri(mCurrentBean.getmFloorImage()));
					mTouchableIv.updatePoints(mCurrentBean.getmListPoint());

				}else{
					mTouchableIv.reset();
					mTouchableIv.setImage(ImageSource.resource(R.drawable.trans));
					mTouchableIv.updatePoints(mCurrentBean.getmListPoint());
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		mAddImageCd=findViewById(R.id.add_image_cd);

		mAddImageCd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(mlistFloor.isEmpty())
				{
					Toast.makeText(SiteMapActivity.this,"Please Add Floor.",Toast.LENGTH_SHORT).show();
				}else{
					Matisse.from(SiteMapActivity.this)
							.choose(MimeType.ofImage())
							.countable(true)
							.capture(false)
							.captureStrategy(
									new CaptureStrategy(true, "com.zhihu.matisse.sample.fileprovider", "test"))
							.maxSelectable(1)
							.addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
							.gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
							.restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
							.thumbnailScale(0.85f)
							.imageEngine(new GlideEngine())
							.showPreview(false) // Default is `true`
							.forResult(REQUEST_CODE_CHOOSE);
				}

			}
		});


		mCreateTourTv=findViewById(R.id.create_tour_tv);
		mCreateTourTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mTouchableIv.setVisibility(View.VISIBLE);
			}
		});

		mTouchableIv=findViewById(R.id.touchble_image);


		final GestureDetector gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onSingleTapConfirmed(MotionEvent e) {
				if (mTouchableIv.isReady()) {
					sCoord = mTouchableIv.viewToSourceCoord(e.getX(), e.getY());

					Intent i=new Intent(SiteMapActivity.this,PhotoTypeSelectionActivity.class);
					startActivityForResult(i,105);

				}
				return true;
			}
		});
		mTouchableIv.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				return gestureDetector.onTouchEvent(motionEvent);
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
			List<String> paths = Matisse.obtainPathResult(data);
			List<Uri> uris = Matisse.obtainResult(data);

			try {
				mRealm.beginTransaction();
				mCurrentBean.setmFloorImage(paths.get(0));
				mRealm.commitTransaction();

				Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uris.get(0));
				mTouchableIv.setImage(ImageSource.bitmap(bitmap));
			} catch (IOException e) {
				e.printStackTrace();
			}

		}else if(requestCode==105 && resultCode==RESULT_OK)
		{
			String	mPhotoUrl=data.getStringExtra("PhotoUrl");
			String mPhotoName=data.getStringExtra("PhotoName");
			String mColors=data.getStringExtra("Colors");
			String mType=data.getStringExtra("Type");

			PointsBean mPointBean=new PointsBean();
			mPointBean.setmPointId("Point_"+UUID.randomUUID());
			mPointBean.setmFloorId(mCurrentBean.getmId());
			mPointBean.setY(sCoord.y);
			mPointBean.setX(sCoord.x);
			mPointBean.setmOrderNumber(0);
			mPointBean.setmImageURL(mPhotoUrl);
			mPointBean.setmImageName(mPhotoName);
			mPointBean.setmImageType(mType);
			mPointBean.setmImageColor(mColors);

			mRealm.beginTransaction();
			RealmList<PointsBean> mlist= (RealmList<PointsBean>) mCurrentBean.getmListPoint();
			mlist.add(mPointBean);
			mCurrentBean.setmListPoint(mlist);
			mRealm.commitTransaction();
			mTouchableIv.updatePoints(mlist);
			mTouchableIv.invalidate();

		}

	}

	public void showDialog(Activity activity, String msg){
		final Dialog dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.setContentView(R.layout.dialog_add_floor);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		dialog.getWindow().setAttributes(lp);


		final EditText mFloorNameEt=dialog.findViewById(R.id.floor_name_et);
		CardView mCancelCv=dialog.findViewById(R.id.cancel_cv);
		CardView mAddCv=dialog.findViewById(R.id.add_cv);


		mCancelCv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dialog.cancel();
			}
		});

		mAddCv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(mFloorNameEt.getText().toString().trim().isEmpty())
				{
					Toast.makeText(SiteMapActivity.this,"Floor Name must be entered.",Toast.LENGTH_SHORT).show();
				}else{
					FloorBean mFloor=new FloorBean();
					mFloor.setmId("Floor"+ UUID.randomUUID());
					mFloor.setmName(mFloorNameEt.getText().toString());
					mFloor.setmPropertyId(mBean.getmPropertyId());

					mRealm.beginTransaction();
					mRealm.insertOrUpdate(mFloor);
					mRealm.commitTransaction();

					mFloorSp.post(new Runnable() {
						@Override
						public void run() {
							mSpinnerAdapter.notifyDataSetChanged();
							mFloorSp.invalidate();
						}
					});

					dialog.cancel();
				}
			}
		});


		dialog.show();

	}


}
