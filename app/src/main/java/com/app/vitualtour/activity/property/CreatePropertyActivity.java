package com.app.vitualtour.activity.property;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.app.vitualtour.R;
import com.app.vitualtour.activity.sitemap.SiteMapActivity;
import com.app.vitualtour.realm.PropertyBean;
import com.app.vitualtour.realm.RealmController;
import com.app.vitualtour.utils.FileUtils;
import com.app.vitualtour.view.GifSizeFilter;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

public class CreatePropertyActivity extends AppCompatActivity {

	private ImageView mPickImage;
	private static final int REQUEST_CODE_CHOOSE = 23;
	private ImageView mPropertyIv;
	private TextView mBackTv;

	private TextView mCreateTourTv;
	LocationManager locationManager;

	private EditText mLatitudeEt;
	private EditText mLongitudeEt;
	private EditText mPropertyNameEt;
	private EditText mPropertyAddressEt;
	private EditText mNumFloorEt;
	List<String> paths=new ArrayList<>();
	List<Uri> uris=new ArrayList<>();

	Realm mRealm;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_property);

		mPropertyIv = findViewById(R.id.property_iv);
		mPropertyIv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mPickImage.performClick();
			}
		});
		mPickImage = findViewById(R.id.add_image_iv);

		mLatitudeEt= findViewById(R.id.latitude_et);
		mLongitudeEt= findViewById(R.id.longitude_et);

		mRealm= RealmController.getInstance().getRealm();

		mPickImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				TedPermission.with(CreatePropertyActivity.this)
						.setPermissionListener(permissionlistener)
						.setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
						.setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
						.check();

			}
		});

		TedPermission.with(CreatePropertyActivity.this)
				.setPermissionListener(permissionlistenerlocation)
				.setDeniedMessage("If you reject per3ymission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
				.setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
				.check();

		mBackTv = findViewById(R.id.back_tv);
		mBackTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		mPropertyNameEt=findViewById(R.id.propertyname_et);
		mPropertyAddressEt=findViewById(R.id.propertyaddress_et);
		mNumFloorEt=findViewById(R.id.num_floor_et);

		mCreateTourTv=findViewById(R.id.create_tour_tv);
		mCreateTourTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(mPropertyNameEt.getText().toString().trim().isEmpty())
				{
					Toast.makeText(CreatePropertyActivity.this,"Property",Toast.LENGTH_SHORT);
				}else if(mPropertyAddressEt.getText().toString().trim().isEmpty())
				{
					Toast.makeText(CreatePropertyActivity.this,"",Toast.LENGTH_SHORT);
				}else if(mNumFloorEt.getText().toString().trim().isEmpty())
				{
					Toast.makeText(CreatePropertyActivity.this,"",Toast.LENGTH_SHORT);
				}else if(mLatitudeEt.getText().toString().trim().isEmpty())
				{
					Toast.makeText(CreatePropertyActivity.this,"",Toast.LENGTH_SHORT);
				}else if(mLongitudeEt.getText().toString().trim().isEmpty())
				{
					Toast.makeText(CreatePropertyActivity.this,"",Toast.LENGTH_SHORT);
				}else{
					PropertyBean bean=new PropertyBean();
					bean.setmPropertyId("Prop_"+UUID.randomUUID());
					bean.setmAddress(mPropertyAddressEt.getText().toString());
					bean.setmName(mPropertyNameEt.getText().toString());
					bean.setmNumFloor(Integer.parseInt(mNumFloorEt.getText().toString()));
					bean.setMlatitude(Double.parseDouble(mLatitudeEt.getText().toString()));
					bean.setMlongitude(Double.parseDouble(mLongitudeEt.getText().toString()));
					bean.setmImage(FileUtils.copyToCache(CreatePropertyActivity.this,new File(paths.get(0))));

					mRealm.beginTransaction();
					mRealm.insertOrUpdate(bean);
					mRealm.commitTransaction();


					Intent i=new Intent(CreatePropertyActivity.this, SiteMapActivity.class);
					i.putExtra("id",bean.getmPropertyId());
					startActivity(i);
					finish();
				}
			}
		});
	}

	PermissionListener permissionlistener = new PermissionListener() {
		@Override
		public void onPermissionGranted() {
			paths.clear();
			uris.clear();
			Matisse.from(CreatePropertyActivity.this)
					.choose(MimeType.ofImage())
					.countable(false)
					.capture(true)
					.theme(R.style.Matisse_Dracula)
					.captureStrategy(new CaptureStrategy(true, getApplicationContext().getPackageName() + ".provider"))
					.maxSelectable(1)
					.addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
					.gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
					.restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
					.thumbnailScale(0.85f)
					.imageEngine(new GlideEngine())
					.showSingleMediaType(true)
					.showPreview(false) // Default is `true`
					.forResult(REQUEST_CODE_CHOOSE);
		}

		@Override
		public void onPermissionDenied(List<String> deniedPermissions) {
			Toast.makeText(CreatePropertyActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
		}
	};

	PermissionListener permissionlistenerlocation = new PermissionListener() {
		@Override
		public void onPermissionGranted() {
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				OnGPS();
			} else {
				getLocation();
			}
		}

		@Override
		public void onPermissionDenied(List<String> deniedPermissions) {
			Toast.makeText(CreatePropertyActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
			 paths = Matisse.obtainPathResult(data);
			 uris = Matisse.obtainResult(data);

			mPropertyIv.setImageURI(uris.get(0));
			mPickImage.setVisibility(View.GONE);
		}
	}

	private void OnGPS() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new  DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			}
		}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		final AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	private void getLocation() {
		@SuppressLint("MissingPermission")
		Location locationGPS = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if (locationGPS != null) {
			double lat = locationGPS.getLatitude();
			double longi = locationGPS.getLongitude();
		String	latitude = String.valueOf(lat);
			String	longitude = String.valueOf(longi);

			mLatitudeEt.setText(latitude);
			mLongitudeEt.setText(longitude);

		} else {
			Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
		}
	}
}
