package com.app.vitualtour.activity.orderslink;

import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.vitualtour.R;
import com.app.vitualtour.adapter.CustomAdapter;
import com.app.vitualtour.realm.FloorBean;
import com.app.vitualtour.realm.PointsBean;
import com.app.vitualtour.realm.PropertyBean;
import com.app.vitualtour.realm.RealmController;
import com.app.vitualtour.view.PinView;
import com.davemorrissey.labs.subscaleview.ImageSource;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import io.realm.Realm;
import io.realm.RealmList;

public class AddOrderActivity extends AppCompatActivity {
	Spinner mFloorSp;
	TextView mBackTv;
	CustomAdapter mSpinnerAdapter;
	List<FloorBean> mlistFloor = new ArrayList<>();
	FloorBean mCurrentBean;
	PropertyBean mBean;
	PinView mTouchableIv;
	private RealmList<PointsBean> mListPoint;
	Realm mRealm;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_orders);

		mRealm=RealmController.getInstance().getRealm();

		mBean = RealmController.getInstance().getProperty(getIntent().getExtras().getString("id"));

		mBackTv = findViewById(R.id.back_tv);
		mBackTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});
		mFloorSp = findViewById(R.id.floor_sp);

		mlistFloor = RealmController.getInstance().getFloorByProp(mBean.getmPropertyId());
		mSpinnerAdapter = new CustomAdapter(this, mlistFloor);
		mFloorSp.setAdapter(mSpinnerAdapter);

		mTouchableIv = findViewById(R.id.touchble_image);
		mTouchableIv.setImage(ImageSource.resource(R.drawable.trans));
		mTouchableIv.setMaxScale(10);

		mFloorSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				mCurrentBean = mlistFloor.get(i);
				mListPoint = (RealmList<PointsBean>) mCurrentBean.getmListPoint();

				if (mCurrentBean != null && mCurrentBean.getmFloorImage() != null) {
					mTouchableIv.setImage(ImageSource.uri(mCurrentBean.getmFloorImage()));
					mTouchableIv.updatePoints(mCurrentBean.getmListPoint());

				} else {
					mTouchableIv.reset();
					mTouchableIv.setImage(ImageSource.resource(R.drawable.trans));
					mTouchableIv.updatePoints(mCurrentBean.getmListPoint());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		final GestureDetector gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {

			@Override
			public boolean onContextClick(MotionEvent e) {
				return super.onContextClick(e);
			}

			@Override
			public boolean onSingleTapConfirmed(MotionEvent e) {
				if (mTouchableIv.isReady()) {
 					for (int i = 0; i < mListPoint.size(); i++) {
						PointF deeplinkCoordinate = mTouchableIv.sourceToViewCoord(mListPoint.get(i).getX(),mListPoint.get(i).getY());
						float deeplinkX = deeplinkCoordinate.x - (mTouchableIv.spinWidth() / 2);
						float deeplinkY = deeplinkCoordinate.y + 20 - mTouchableIv.spinHeight();
					    PointF pointF = mTouchableIv.viewToSourceCoord(e.getX(), e.getY());
						PointF tappedCoordinate = mTouchableIv.sourceToViewCoord(pointF);
						if (tappedCoordinate.x >= deeplinkX && tappedCoordinate.x < deeplinkX + mTouchableIv.spinWidth()
								&& tappedCoordinate.y >= deeplinkY && tappedCoordinate.y < deeplinkY + mTouchableIv.spinHeight()) {

							if(mListPoint.get(i).getmOrderNumber()==0)
							{
								int number=RealmController.getInstance().getMaxNumber(mListPoint.get(i).getmFloorId());
								Log.d("number",""+number);

								mRealm.beginTransaction();
								mListPoint.get(i).setmOrderNumber(number+1);
								mRealm.copyToRealmOrUpdate(mListPoint.get(i));
								mRealm.commitTransaction();
								mTouchableIv.invalidate();
							}
						}
					}
				}
				return true;
			}
		});


		mTouchableIv.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				return gestureDetector.onTouchEvent(motionEvent);
			}
		});
	}

	boolean isInside(double distanceX, double distanceY, float radius) {
		return ((distanceX * distanceX) + (distanceY * distanceY)) <= radius * radius;
	}

	private boolean inCircle(float x, float y, float circleCenterX, float circleCenterY, float circleRadius) {
		double dx = Math.pow(x - circleCenterX, 2);
		double dy = Math.pow(y - circleCenterY, 2);

		if ((dx + dy) < Math.pow(circleRadius, 2)) {
			return true;
		} else {
			return false;
		}
	}

	boolean isInside(float circle_x, float circle_y,
	                 float rad, float x, float y) {
		double d = Math.sqrt(Math.pow(circle_x - x, 2) + Math.pow(circle_y - y, 2));
		double dist = Math.sqrt((circle_x - x) * 2 + (circle_y - y) * 2);
		return dist <= rad;
	}


}
