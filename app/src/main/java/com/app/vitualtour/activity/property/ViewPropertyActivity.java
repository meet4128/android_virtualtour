package com.app.vitualtour.activity.property;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.app.vitualtour.R;
import com.app.vitualtour.activity.sitemap.SiteMapActivity;
import com.app.vitualtour.realm.PropertyBean;
import com.app.vitualtour.realm.RealmController;
import com.app.vitualtour.utils.FileUtils;
import com.app.vitualtour.view.GifSizeFilter;
import com.bumptech.glide.Glide;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

public class ViewPropertyActivity extends AppCompatActivity {

	PropertyBean mBean;
	private ImageView mPropertyIv;
	private TextView mNameTv;
	private TextView mAddressTv;
	private TextView mFloorTv;
	private TextView mLatitudeTv;
	private TextView mLongitudeTv;

	private TextView mTitleTv;
	private TextView mBackTv;
	private static final int REQUEST_CODE_CHOOSE = 23;
	private CardView mUpdateProperty;
	private CardView mCreateTour;
	List<String> paths=new ArrayList<>();
	List<Uri> uris=new ArrayList<>();

	private Realm mRealm;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_property);

		mBean= RealmController.getInstance().getProperty(getIntent().getExtras().getString("bean")) ;

		mTitleTv=findViewById(R.id.title_tv);
		mTitleTv.setText(mBean.getmName());




		mRealm=RealmController.getInstance().getRealm();

		mBackTv=findViewById(R.id.back_tv);
		mBackTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		mNameTv=findViewById(R.id.name_et);
		mAddressTv=findViewById(R.id.address_et);
		mFloorTv=findViewById(R.id.floor_et);
		mLatitudeTv=findViewById(R.id.latitude_et);
		mLongitudeTv=findViewById(R.id.longitude_et);

		mPropertyIv=findViewById(R.id.property_iv);
		Glide.with(this).load(mBean.getmImage()).into(mPropertyIv);

		mNameTv.setText(mBean.getmName());
		mAddressTv.setText(mBean.getmAddress());
		mFloorTv.setText(""+mBean.getmNumFloor());
		mLatitudeTv.setText(""+mBean.getMlatitude());
		mLongitudeTv.setText(""+mBean.getMlongitude());

		mUpdateProperty=findViewById(R.id.add_property_card);

		mPropertyIv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TedPermission.with(ViewPropertyActivity.this)
						.setPermissionListener(permissionlistener)
						.setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
						.setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
						.check();
			}
		});

		mUpdateProperty.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {



				if(mNameTv.getText().toString().trim().isEmpty())
				{
					Toast.makeText(ViewPropertyActivity.this,"Property",Toast.LENGTH_SHORT);
				}else if(mAddressTv.getText().toString().trim().isEmpty())
				{
					Toast.makeText(ViewPropertyActivity.this,"",Toast.LENGTH_SHORT);
				}else if(mFloorTv.getText().toString().trim().isEmpty())
				{
					Toast.makeText(ViewPropertyActivity.this,"",Toast.LENGTH_SHORT);
				}else if(mLatitudeTv.getText().toString().trim().isEmpty())
				{
					Toast.makeText(ViewPropertyActivity.this,"",Toast.LENGTH_SHORT);
				}else if(mLongitudeTv.getText().toString().trim().isEmpty())
				{
					Toast.makeText(ViewPropertyActivity.this,"",Toast.LENGTH_SHORT);
				}else{

					mRealm.beginTransaction();
					mBean.setmAddress(mAddressTv.getText().toString());
					mBean.setmName(mNameTv.getText().toString());
					mBean.setmNumFloor(Integer.parseInt(mFloorTv.getText().toString()));
					mBean.setMlatitude(Double.parseDouble(mLatitudeTv.getText().toString()));
					mBean.setMlongitude(Double.parseDouble(mLongitudeTv.getText().toString()));
					if(!paths.isEmpty())
					{
						mBean.setmImage(FileUtils.copyToCache(ViewPropertyActivity.this,new File(paths.get(0))));
					}
					mRealm.insertOrUpdate(mBean);
					mRealm.commitTransaction();
					finish();
				}
			}
		});


		mCreateTour=findViewById(R.id.create_tour);
		mCreateTour.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent i=new Intent(ViewPropertyActivity.this, SiteMapActivity.class);
				i.putExtra("id",mBean.getmPropertyId());
				startActivity(i);
				finish();
			}
		});
	}

	PermissionListener permissionlistener = new PermissionListener() {
		@Override
		public void onPermissionGranted() {
			paths.clear();
			uris.clear();
			Matisse.from(ViewPropertyActivity.this)
					.choose(MimeType.ofImage())
					.countable(true)
					.capture(false)
					.captureStrategy(
							new CaptureStrategy(true, "com.zhihu.matisse.sample.fileprovider", "test"))
					.maxSelectable(1)
					.addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
					.gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
					.restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
					.thumbnailScale(0.85f)
					.imageEngine(new GlideEngine())
					.showPreview(false) // Default is `true`
					.forResult(REQUEST_CODE_CHOOSE);
		}

		@Override
		public void onPermissionDenied(List<String> deniedPermissions) {
			Toast.makeText(ViewPropertyActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
			paths = Matisse.obtainPathResult(data);
			uris = Matisse.obtainResult(data);

			mPropertyIv.setImageURI(uris.get(0));
		}
	}
}
