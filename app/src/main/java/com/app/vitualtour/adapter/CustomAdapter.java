package com.app.vitualtour.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.vitualtour.R;
import com.app.vitualtour.realm.FloorBean;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<FloorBean> {

    LayoutInflater flater;

    public CustomAdapter(Activity context, List<FloorBean> list){

        super(context,R.layout.item_spinner, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return rowview(convertView,position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView,position);
    }

    private View rowview(View convertView , int position){

        FloorBean rowItem = getItem(position);

        viewHolder holder ;
        View rowview = convertView;
        if (rowview==null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.item_spinner, null, false);

            holder.txtTitle = (TextView) rowview.findViewById(R.id.textViewSpinnerItem);
            rowview.setTag(holder);
        }else{
            holder = (viewHolder) rowview.getTag();
        }
        holder.txtTitle.setText(rowItem.getmName());

        return rowview;
    }

    private class viewHolder{
        TextView txtTitle;
    }
}
