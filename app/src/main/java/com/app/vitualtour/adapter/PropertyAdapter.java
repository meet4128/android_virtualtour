package com.app.vitualtour.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.vitualtour.R;
import com.app.vitualtour.realm.PropertyBean;
import com.bumptech.glide.Glide;

import java.util.List;

public class PropertyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private List<PropertyBean> mPropertyList;
	private Context mContext;

	public interface OnItemClickListener {
		void onItemClick(String pos);
		void onDeteleClick(String pos);
	}
	private OnItemClickListener listener;

	public PropertyAdapter(Context mContext, List<PropertyBean> mPropertyList, OnItemClickListener listener) {
		this.mPropertyList = mPropertyList;
		this.mContext = mContext;
		this.listener=listener;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.item_property, parent, false);


		return new MainListItem(view);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
		MainListItem mainListItem = (MainListItem) holder;

		final PropertyBean mBean = mPropertyList.get(position);

		mainListItem.mNameTv.setText(mBean.getmName());
		mainListItem.mAddressTv.setText(mBean.getmAddress());

		Glide.with(mContext).load(mBean.getmImage()).into(mainListItem.mPropertyIv);

		mainListItem.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listener.onItemClick(mBean.getmPropertyId());
			}
		});

		mainListItem.mDeleteTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				listener.onDeteleClick(mBean.getmPropertyId());
			}
		});
	}

	@Override
	public int getItemCount() {
		return mPropertyList.size();
	}

	static class MainListItem extends RecyclerView.ViewHolder {


		private TextView mNameTv;
		private TextView mAddressTv;
		private TextView mTimeTv;
		private ImageView mPropertyIv;
		private TextView mDeleteTv;

		protected MainListItem(View itemView) {
			super(itemView);

			mNameTv = itemView.findViewById(R.id.name_tv);
			mAddressTv = itemView.findViewById(R.id.address_tv);
			mTimeTv = itemView.findViewById(R.id.time_tv);
			mPropertyIv = itemView.findViewById(R.id.property_iv);
			mDeleteTv=itemView.findViewById(R.id.delete_tv);
		}
	}
}