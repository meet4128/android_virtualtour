package com.app.vitualtour.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;

import com.app.vitualtour.R;
import com.app.vitualtour.realm.PointsBean;
import com.app.vitualtour.realm.RealmController;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.util.List;

import io.realm.Realm;

public class PinView extends SubsamplingScaleImageView {

	private final Paint paint = new Paint();
	private final Paint paintText = new Paint();
	private final PointF vPin = new PointF();

	private Bitmap pinRed;
	private Bitmap pinBlue;
	private Bitmap pinGreen;
	private Bitmap pinYellow;
	private Bitmap pinPink;

	private int icon_red = R.drawable.ic_red_ci;
	private int icon_blue = R.drawable.ic_blue_ci ;
	private int icon_green = R.drawable.ic_green_ci;
	private int icon_yellow = R.drawable.ic_yellow_ci;
	private int icon_pink = R.drawable.ic_pink_ci;



    private List<PointsBean> sPoints;
    private Realm mRealm;

    float spinWidth=0;
    float spinHeight=0;

	public PinView(Context context) {
		this(context, null);
        mRealm= RealmController.getInstance().getRealm();
	}

	public PinView(Context context, AttributeSet attr) {
		super(context, attr);
        mRealm= RealmController.getInstance().getRealm();
		initialise();

	}

	private void initialise() {
		pinRed = BitmapFactory.decodeResource(this.getResources(), icon_red);
		pinBlue= BitmapFactory.decodeResource(this.getResources(), icon_blue);
		pinGreen= BitmapFactory.decodeResource(this.getResources(), icon_green);
		pinYellow= BitmapFactory.decodeResource(this.getResources(), icon_yellow);
		pinPink= BitmapFactory.decodeResource(this.getResources(), icon_pink);

		spinWidth = pinRed.getWidth() + 20;
		spinHeight = pinRed.getHeight() + 20;
		pinRed = Bitmap.createScaledBitmap(pinRed, (int) spinWidth, (int) spinHeight, true);

		spinWidth = pinBlue.getWidth() + 20;
		spinHeight = pinBlue.getHeight() + 20;
		pinBlue = Bitmap.createScaledBitmap(pinBlue, (int) spinWidth, (int) spinHeight, true);

		spinWidth = pinGreen.getWidth() + 20;
		spinHeight = pinGreen.getHeight() + 20;
		pinGreen = Bitmap.createScaledBitmap(pinGreen, (int) spinWidth, (int) spinHeight, true);

		spinWidth = pinYellow.getWidth() + 20;
		spinHeight = pinYellow.getHeight() + 20;
		pinYellow = Bitmap.createScaledBitmap(pinYellow, (int) spinWidth, (int) spinHeight, true);

		spinWidth = pinPink.getWidth() + 20;
		spinHeight = pinPink.getHeight() + 20;
		pinPink = Bitmap.createScaledBitmap(pinPink, (int) spinWidth, (int) spinHeight, true);

		paintText.setAntiAlias(true);
		paintText.setTextSize(16 * getResources().getDisplayMetrics().density);
		paintText.setColor(0xFFffffff);
	}

    public float spinWidth()
    {
        return spinWidth;
    }

    public float spinHeight()
    {
        return spinHeight;
    }

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (!isReady()) {
			return;
		}

        if(sPoints==null)
            return;

        paint.setAntiAlias(true);

        for(int i=0;i<sPoints.size();i++)
        {
            sourceToViewCoord(sPoints.get(i).getX(),sPoints.get(i).getY(),vPin);
            float vX = vPin.x - (spinWidth/2);
            float vY = vPin.y - spinHeight;
	        if(sPoints.get(i).getmImageColor().equals("#1fff09"))
	        {
		        canvas.drawBitmap(pinGreen, vX, vY, paint);
	        }else  if(sPoints.get(i).getmImageColor().equals("#ff0000"))
	        {
		        canvas.drawBitmap(pinRed, vX, vY, paint);
	        }else  if(sPoints.get(i).getmImageColor().equals("#ff09ee"))
	        {
		        canvas.drawBitmap(pinPink, vX, vY, paint);
	        }else  if(sPoints.get(i).getmImageColor().equals("#fff009"))
	        {
		        canvas.drawBitmap(pinYellow, vX, vY, paint);
	        }else  if(sPoints.get(i).getmImageColor().equals("#0a09ff"))
	        {
		        canvas.drawBitmap(pinBlue, vX, vY, paint);
	        }
            if(sPoints.get(i).getmOrderNumber()!=0){
	            canvas.drawText(String.valueOf(sPoints.get(i).getmOrderNumber()), vX+(spinWidth/2-10), vY+(spinHeight/2)+30, paintText);
            }
        }
	}

    public void reset()
    {
        sPoints=null;
    }

    public void updatePoints(List<PointsBean> sPoints)
    {
        this.sPoints=sPoints;
    }
}

