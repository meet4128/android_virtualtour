package com.app.vitualtour.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;

import com.app.vitualtour.R;
import com.app.vitualtour.model.LinesResult;
import com.app.vitualtour.realm.FloorBean;
import com.app.vitualtour.realm.LineBeans;
import com.app.vitualtour.realm.PointsBean;
import com.app.vitualtour.realm.RealmController;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;

public class PinViewLines extends SubsamplingScaleImageView implements View.OnTouchListener {

	private final Paint paint = new Paint();
	private final Paint paintText = new Paint();
	private final PointF vPin = new PointF();
	Paint paintLine= new Paint();
	private Bitmap pinRed;
	private Bitmap pinBlue;
	private Bitmap pinGreen;
	private Bitmap pinYellow;
	private Bitmap pinPink;

	private int icon_red = R.drawable.ic_red_ci;
	private int icon_blue = R.drawable.ic_blue_ci ;
	private int icon_green = R.drawable.ic_green_ci;
	private int icon_yellow = R.drawable.ic_yellow_ci;
	private int icon_pink = R.drawable.ic_pink_ci;

	FloorBean mCurrentBean;

    private List<PointsBean> sPoints;
	private List<LineBeans> sLines;
    private Realm mRealm;

    float spinWidth=0;
    float spinHeight=0;


	private PointF vStart;
	private PointF vEnd;

	public PinViewLines(Context context) {
		this(context, null);
        mRealm= RealmController.getInstance().getRealm();
	}

	public PinViewLines(Context context, AttributeSet attr) {
		super(context, attr);
        mRealm= RealmController.getInstance().getRealm();
		initialise();

	}

	private void initialise() {
		pinRed = BitmapFactory.decodeResource(this.getResources(), icon_red);
		pinBlue= BitmapFactory.decodeResource(this.getResources(), icon_blue);
		pinGreen= BitmapFactory.decodeResource(this.getResources(), icon_green);
		pinYellow= BitmapFactory.decodeResource(this.getResources(), icon_yellow);
		pinPink= BitmapFactory.decodeResource(this.getResources(), icon_pink);

		paintLine = new Paint();
		paintLine.setColor(Color.RED);
		paintLine.setAntiAlias(true);
		paintLine.setStyle(Paint.Style.STROKE);
		paintLine.setStrokeWidth(14);

		spinWidth = pinRed.getWidth() + 20;
		spinHeight = pinRed.getHeight() + 20;
		pinRed = Bitmap.createScaledBitmap(pinRed, (int) spinWidth, (int) spinHeight, true);

		spinWidth = pinBlue.getWidth() + 20;
		spinHeight = pinBlue.getHeight() + 20;
		pinBlue = Bitmap.createScaledBitmap(pinBlue, (int) spinWidth, (int) spinHeight, true);

		spinWidth = pinGreen.getWidth() + 20;
		spinHeight = pinGreen.getHeight() + 20;
		pinGreen = Bitmap.createScaledBitmap(pinGreen, (int) spinWidth, (int) spinHeight, true);

		spinWidth = pinYellow.getWidth() + 20;
		spinHeight = pinYellow.getHeight() + 20;
		pinYellow = Bitmap.createScaledBitmap(pinYellow, (int) spinWidth, (int) spinHeight, true);

		spinWidth = pinPink.getWidth() + 20;
		spinHeight = pinPink.getHeight() + 20;
		pinPink = Bitmap.createScaledBitmap(pinPink, (int) spinWidth, (int) spinHeight, true);

		paintText.setAntiAlias(true);
		paintText.setTextSize(16 * getResources().getDisplayMetrics().density);
		paintText.setColor(0xFFffffff);
	}

    public float spinWidth()
    {
        return spinWidth;
    }

    public float spinHeight()
    {
        return spinHeight;
    }

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (!isReady()) {
			return;
		}

        if(sPoints==null)
            return;

        paint.setAntiAlias(true);

		if(sLines!=null)
		{
			for(int i=0;i<sLines.size();i++)
			{
				PointF start=sourceToViewCoord(sLines.get(i).getStartX(), sLines.get(i).getStartY());
				PointF end=sourceToViewCoord(sLines.get(i).getEndX(),sLines.get(i).getEndY());

				if(sLines.get(i).getmLineColor().equals("#1fff09"))
				{
					paintLine.setColor(Color.parseColor("#1fff09"));
				}else  if(sLines.get(i).getmLineColor().equals("#ff0000"))
				{
					paintLine.setColor(Color.parseColor("#ff0000"));
				}else  if(sLines.get(i).getmLineColor().equals("#ff09ee"))
				{
					paintLine.setColor(Color.parseColor("#ff09ee"));
				}else  if(sLines.get(i).getmLineColor().equals("#fff009"))
				{
					paintLine.setColor(Color.parseColor("#fff009"));
				}else  if(sLines.get(i).getmLineColor().equals("#0a09ff"))
				{
					paintLine.setColor(Color.parseColor("#0a09ff"));
				}
				canvas.drawLine(start.x, start.y,end.x,end.y , paintLine);

			}
		}

        for(int i=0;i<sPoints.size();i++)
        {
            sourceToViewCoord(sPoints.get(i).getX(),sPoints.get(i).getY(),vPin);
            float vX = vPin.x - (spinWidth/2);
            float vY = vPin.y - spinHeight;

            if(sPoints.get(i).getmImageColor().equals("#1fff09"))
            {
	            canvas.drawBitmap(pinGreen, vX, vY, paint);
            }else  if(sPoints.get(i).getmImageColor().equals("#ff0000"))
            {
	            canvas.drawBitmap(pinRed, vX, vY, paint);
            }else  if(sPoints.get(i).getmImageColor().equals("#ff09ee"))
            {
	            canvas.drawBitmap(pinPink, vX, vY, paint);
            }else  if(sPoints.get(i).getmImageColor().equals("#fff009"))
            {
	            canvas.drawBitmap(pinYellow, vX, vY, paint);
            }else  if(sPoints.get(i).getmImageColor().equals("#0a09ff"))
            {
	            canvas.drawBitmap(pinBlue, vX, vY, paint);
            }


            if(sPoints.get(i).getmOrderNumber()!=0){
	            canvas.drawText(String.valueOf(sPoints.get(i).getmOrderNumber()), vX+(spinWidth/2-10), vY+(spinHeight/2)+30, paintText);
            }
        }



	}

    public void reset()
    {
        sPoints=null;
    }

    public void updatePoints(List<PointsBean> sPoints)
    {
        this.sPoints=sPoints;
    }

	public void updateLines(List<LineBeans> sPoints)
	{
		this.sLines=sPoints;
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		return false;
	}

	@Override
	public boolean onTouchEvent(@NonNull MotionEvent event) {
		if (sPoints == null) {
			return super.onTouchEvent(event);
		}
		switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				if (event.getActionIndex() == 0) {
					vStart = new PointF(event.getX(), event.getY());
					//isFirstPoint(vStart);
				} else {
					vStart = null;
				}
				Log.d("line","start"+vStart.x);
				return true;
			case MotionEvent.ACTION_UP:
				vEnd = new PointF(event.getX(), event.getY());
				//isSecondPoint(vEnd);

				if(isFirstPoint(vStart).isContain() && isSecondPoint(vEnd).isContain())
				{

					PointF startResult=isFirstPoint(vStart).getmCurrentPoint();
					PointF endResult=isSecondPoint(vEnd).getmCurrentPoint();

					LineBeans bean=new LineBeans();
					bean.setmFloorId(sPoints.get(0).getmFloorId());
					bean.setmLineId("Line_"+ UUID.randomUUID());

					PointF start= viewToSourceCoord(startResult.x, startResult.y);
					bean.setStartX(start.x);
					bean.setStartY(start.y);
					PointF end= viewToSourceCoord(endResult.x, endResult.y);
					bean.setEndX(end.x);
					bean.setEndY(end.y);
					bean.setmLineColor(isFirstPoint(vStart).getLineColor());


 					LineBeans oldone=RealmController.getInstance().deleteLine(start.x,start.y,end.x,end.y);

					if(oldone!=null)
					{
						mRealm.beginTransaction();
						RealmList<LineBeans> mlist= (RealmList<LineBeans>) mCurrentBean.getmListLines();
						if(mlist.size()!=0){
							mlist.remove(oldone);
						}
						sLines=mlist;
						oldone.deleteFromRealm();
						mRealm.commitTransaction();

						invalidate();
					}else{
						mRealm.beginTransaction();
						RealmList<LineBeans> mlist= (RealmList<LineBeans>) mCurrentBean.getmListLines();
						mlist.add(bean);
						mCurrentBean.setmListLines(mlist);
						mRealm.copyToRealmOrUpdate(mlist);
						mRealm.commitTransaction();
						sLines=mlist;
						invalidate();
					}

				}

				Log.d("line","up"+vEnd.x);
				break;
		}
		return  super.onTouchEvent(event);
	}

	private LinesResult isFirstPoint(PointF vStart)
	{
		LinesResult mresult=new LinesResult();
		boolean isContain=false;
		for (int i = 0; i < sPoints.size(); i++) {
			PointF deeplinkCoordinate = sourceToViewCoord(sPoints.get(i).getX(),sPoints.get(i).getY());
			float deeplinkX = deeplinkCoordinate.x - (spinWidth() / 2);
			float deeplinkY = deeplinkCoordinate.y + 20 - spinHeight();
			PointF pointF = viewToSourceCoord(vStart.x, vStart.y);
			PointF tappedCoordinate = sourceToViewCoord(pointF);
			if (tappedCoordinate.x >= deeplinkX && tappedCoordinate.x < deeplinkX + spinWidth()
					&& tappedCoordinate.y >= deeplinkY && tappedCoordinate.y < deeplinkY + spinHeight()) {
				mresult.setContain(true);
				mresult.setmCurrentPoint(deeplinkCoordinate);
				mresult.setLineColor(sPoints.get(i).getmImageColor());
			}
		}
		return mresult;
	}

	private LinesResult isSecondPoint(PointF vStart)
	{
		LinesResult mresult=new LinesResult();
		for (int i = 0; i < sPoints.size(); i++) {
			PointF deeplinkCoordinate = sourceToViewCoord(sPoints.get(i).getX(),sPoints.get(i).getY());
			float deeplinkX = deeplinkCoordinate.x - (spinWidth() / 2);
			float deeplinkY = deeplinkCoordinate.y + 20 - spinHeight();
			PointF pointF = viewToSourceCoord(vStart.x, vStart.y);
			PointF tappedCoordinate = sourceToViewCoord(pointF);
			if (tappedCoordinate.x >= deeplinkX && tappedCoordinate.x < deeplinkX + spinWidth()
					&& tappedCoordinate.y >= deeplinkY && tappedCoordinate.y < deeplinkY + spinHeight()) {
				mresult.setContain(true);
				mresult.setmCurrentPoint(deeplinkCoordinate);
				mresult.setLineColor(sPoints.get(i).getmImageColor());
				break;
			}
		}
		return mresult;
	}

	public void setCurrentFloor(FloorBean mCurrentBean)
	{
		this.mCurrentBean=mCurrentBean;
	}
}

