package com.app.vitualtour.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;

import com.app.vitualtour.realm.PointsBean;
import com.app.vitualtour.realm.RealmController;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import io.realm.Realm;

public class DefaltCircle extends SubsamplingScaleImageView {

    private int strokeWidth;

    private List<PointsBean> sPoints;
    private final Paint paint = new Paint();
    private final Paint paintText = new Paint();
    private Realm mRealm;

    public DefaltCircle(Context context) {
        this(context, null);
        mRealm= RealmController.getInstance().getRealm();
    }

    public DefaltCircle(Context context, AttributeSet attr) {
        super(context, attr);
        mRealm= RealmController.getInstance().getRealm();
        initialise();
    }

    private void initialise() {
        float density = getResources().getDisplayMetrics().densityDpi;
        strokeWidth = (int)(density/60f);
        paintText.setColor(Color.RED);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Don't draw pin before image is ready so it doesn't move around during setup.
        if (!isReady()) {
            return;
        }


        if(sPoints==null)
            return;


        float radius = (getScale() * getSWidth()) * 0.03f;
        Log.d("radius",""+radius);

        for(int i=0;i<sPoints.size();i++)
        {
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.FILL);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStrokeWidth(strokeWidth * 2);
            paint.setColor(Color.parseColor(sPoints.get(i).getmImageColor()));
            paintText.setTextSize(radius);
            PointF pointf=sourceToViewCoord(sPoints.get(i).getX(),sPoints.get(i).getY());
            canvas.drawCircle(pointf.x, pointf.y, radius, paint);
            //canvas.drawText("0",pointf.x-(radius), pointf.y, paintText);
        }
    }

    public void addPoints(PointsBean points)
    {
        if(sPoints==null)
            sPoints=new ArrayList<>();

        if(points!=null)
        {
            mRealm.beginTransaction();
            sPoints.add(points);
            mRealm.commitTransaction();
        }

    }

    public void reset()
    {
        sPoints=null;
    }

    public void updatePoints(List<PointsBean> sPoints)
    {
      this.sPoints=sPoints;
    }


}