package com.app.vitualtour.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.app.vitualtour.activity.sitemap.PhotoTypeSelectionActivity;
import com.app.vitualtour.activity.sitemap.SiteMapActivity;

import java.util.ArrayList;
import java.util.List;

public class TouchableImageView extends AppCompatImageView {

    public static final String BUNDLE_SUPER_STATE = "bundle_super_state";
    public static final String BUNDLE_POINT_LIST = "bundle_point_list";

    private float mRadius;
    private Paint myCirclePaint = null;
    private List<PointF> mPoints;
    private GestureDetector mDetector;
    private Context context;


    public TouchableImageView(Context context) {
        super(context);
        init(context);
    }

    public TouchableImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TouchableImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.context=context;
        mRadius = 30;
        myCirclePaint = new Paint();
        mPoints = new ArrayList<>();
        mDetector = new GestureDetector(context, new MyListener());
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (mPoints != null) {
            for (int i = 0; i < mPoints.size(); i++) {
                canvas.drawCircle(mPoints.get(i).x, mPoints.get(i).y, mRadius, myCirclePaint);
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        return mDetector.onTouchEvent(event);
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_SUPER_STATE, super.onSaveInstanceState());
        bundle.putParcelableArrayList(BUNDLE_POINT_LIST, (ArrayList<? extends Parcelable>) mPoints);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            mPoints = bundle.getParcelableArrayList(BUNDLE_POINT_LIST);
            super.onRestoreInstanceState(bundle.getParcelable(BUNDLE_SUPER_STATE));
        } else {
            super.onRestoreInstanceState(state);
        }
    }

    class MyListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent event) {
            if (mPoints != null) {
                mPoints.add(new PointF(event.getX(), event.getY()));
                invalidate();
            }
            return true;
        }
        @Override
        public boolean onSingleTapUp(MotionEvent e)
        {
            Intent i=new Intent(context,PhotoTypeSelectionActivity.class);
            context.startActivity(i);
            return true;
        }
    }
}