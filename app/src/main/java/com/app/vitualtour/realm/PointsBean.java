package com.app.vitualtour.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PointsBean extends RealmObject
{
    @PrimaryKey
    private String mPointId;
    private String mFloorId;
    private float x;
    private float y;
    private int mOrderNumber;
    private String mImageURL;
    private String mImageName;
    private String mImageType;
    private String mImageColor;

    public int getmOrderNumber() {
        return mOrderNumber;
    }

    public void setmOrderNumber(int mOrderNumber) {
        this.mOrderNumber = mOrderNumber;
    }

    public String getmImageURL() {
        return mImageURL;
    }

    public void setmImageURL(String mImageURL) {
        this.mImageURL = mImageURL;
    }

    public String getmImageName() {
        return mImageName;
    }

    public void setmImageName(String mImageName) {
        this.mImageName = mImageName;
    }

    public String getmImageType() {
        return mImageType;
    }

    public void setmImageType(String mImageType) {
        this.mImageType = mImageType;
    }

    public String getmImageColor() {
        return mImageColor;
    }

    public void setmImageColor(String mImageColor) {
        this.mImageColor = mImageColor;
    }

    public String getmPointId() {
        return mPointId;
    }

    public void setmPointId(String mPointId) {
        this.mPointId = mPointId;
    }

    public String getmFloorId() {
        return mFloorId;
    }

    public void setmFloorId(String mFloorId) {
        this.mFloorId = mFloorId;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
