package com.app.vitualtour.realm;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PropertyBean extends RealmObject implements Serializable {
	@PrimaryKey
	String mPropertyId;
	String mName;
	String mAddress;
	int mNumFloor;
	double mlatitude;
	double mlongitude;
	String mImage;

	public String getmPropertyId() {
		return mPropertyId;
	}

	public void setmPropertyId(String mPropertyId) {
		this.mPropertyId = mPropertyId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmAddress() {
		return mAddress;
	}

	public void setmAddress(String mAddress) {
		this.mAddress = mAddress;
	}

	public int getmNumFloor() {
		return mNumFloor;
	}

	public void setmNumFloor(int mNumFloor) {
		this.mNumFloor = mNumFloor;
	}

	public double getMlatitude() {
		return mlatitude;
	}

	public void setMlatitude(double mlatitude) {
		this.mlatitude = mlatitude;
	}

	public double getMlongitude() {
		return mlongitude;
	}

	public void setMlongitude(double mlongitude) {
		this.mlongitude = mlongitude;
	}

	public String getmImage() {
		return mImage;
	}

	public void setmImage(String mImage) {
		this.mImage = mImage;
	}

}
