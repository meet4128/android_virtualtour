package com.app.vitualtour.realm;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FloorBean extends RealmObject {

    @PrimaryKey
    private String mId;
    private String mName;
    private String mPropertyId;
    private String mFloorImage;
    private RealmList<PointsBean> mListPoint;
    private RealmList<LineBeans> mListLines;


    public RealmList<LineBeans> getmListLines() {
        return mListLines;
    }

    public void setmListLines(RealmList<LineBeans> mListLines) {
        this.mListLines = mListLines;
    }

    public String getmFloorImage() {
        return mFloorImage;
    }

    public void setmFloorImage(String mFloorImage) {
        this.mFloorImage = mFloorImage;
    }

    public List<PointsBean> getmListPoint() {
        return mListPoint;
    }

    public void setmListPoint(RealmList<PointsBean> mListPoint) {
        this.mListPoint = mListPoint;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPropertyId() {
        return mPropertyId;
    }

    public void setmPropertyId(String mPropertyId) {
        this.mPropertyId = mPropertyId;
    }
}
