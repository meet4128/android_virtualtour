package com.app.vitualtour.realm;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TourBean extends RealmObject
{
    @PrimaryKey
    private  String mTourId;
    private String mPropertyId;
    private RealmList<FloorBean> mlistFloor;


    public String getmTourId() {
        return mTourId;
    }

    public void setmTourId(String mTourId) {
        this.mTourId = mTourId;
    }

    public String getmPropertyId() {
        return mPropertyId;
    }

    public void setmPropertyId(String mPropertyId) {
        this.mPropertyId = mPropertyId;
    }

    public List<FloorBean> getMlistFloor() {
        return mlistFloor;
    }

    public void setMlistFloor(RealmList<FloorBean> mlistFloor) {
        this.mlistFloor = mlistFloor;
    }
}
