package com.app.vitualtour.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LineBeans extends RealmObject
{
	@PrimaryKey
	private String mLineId;
	private String mFloorId;
	private float startX;
	private float startY;
	private float endX;
	private float endY;
	private int toOrder;
	private int fromOrder;
	private String mLineColor;

	public String getmLineColor() {
		return mLineColor;
	}
	public void setmLineColor(String mLineColor) {
		this.mLineColor = mLineColor;
	}
	public int getToOrder() {
		return toOrder;
	}

	public void setToOrder(int toOrder) {
		this.toOrder = toOrder;
	}

	public int getFromOrder() {
		return fromOrder;
	}

	public void setFromOrder(int fromOrder) {
		this.fromOrder = fromOrder;
	}

	public String getmLineId() {
		return mLineId;
	}

	public void setmLineId(String mLineId) {
		this.mLineId = mLineId;
	}

	public String getmFloorId() {
		return mFloorId;
	}

	public void setmFloorId(String mFloorId) {
		this.mFloorId = mFloorId;
	}

	public float getStartX() {
		return startX;
	}

	public void setStartX(float startX) {
		this.startX = startX;
	}

	public float getStartY() {
		return startY;
	}

	public void setStartY(float startY) {
		this.startY = startY;
	}

	public float getEndX() {
		return endX;
	}

	public void setEndX(float endX) {
		this.endX = endX;
	}

	public float getEndY() {
		return endY;
	}

	public void setEndY(float endY) {
		this.endY = endY;
	}
}
