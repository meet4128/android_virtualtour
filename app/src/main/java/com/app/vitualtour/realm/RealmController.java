package com.app.vitualtour.realm;


import android.app.Activity;
import android.app.Application;

import androidx.fragment.app.Fragment;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

	private static RealmController instance;
	private final Realm realm;

	public RealmController(Application application) {
		realm = Realm.getDefaultInstance();
	}

	public static RealmController with(Fragment fragment) {

		if (instance == null) {
			instance = new RealmController(fragment.getActivity().getApplication());
		}
		return instance;
	}

	public static RealmController with(Activity activity) {

		if (instance == null) {
			instance = new RealmController(activity.getApplication());
		}
		return instance;
	}

	public static RealmController with(Application application) {

		if (instance == null) {
			instance = new RealmController(application);
		}
		return instance;
	}

	public static RealmController getInstance() {

		return instance;
	}

	public Realm getRealm() {

		return realm;
	}

	//Refresh the realm istance
	public void refresh() {

		realm.refresh();
	}

	public RealmResults<PropertyBean> getAllProperties() {

		return realm.where(PropertyBean.class).findAll();
	}

	public RealmResults<FloorBean> getFloorByProp(String mPropId) {

		return realm.where(FloorBean.class).equalTo("mPropertyId",mPropId).findAll();
	}


	public PropertyBean getProperty(String id) {

		return realm.where(PropertyBean.class).equalTo("mPropertyId",id).findFirst();
	}


	public TourBean getTour(String mPropertyId)
		{
			return realm.where(TourBean.class).equalTo("mPropertyId",mPropertyId).findFirst();
		}

	public int getMaxNumber(String mFloorId)
	{
		return realm.where(PointsBean.class).equalTo("mFloorId",mFloorId).max("mOrderNumber").intValue();
	}


	public LineBeans deleteLine(float startX,float startY,float endX,float endY)
	{
		return realm.where(LineBeans.class)
				.beginGroup()
				.equalTo("startX",startX)
				.and()
				.equalTo("startY",startY)
				.and()
				.equalTo("endX",endX)
				.and()
				.equalTo("endY",endY)
				.endGroup()
				.or()
				.beginGroup()
				.equalTo("startX",endX)
				.and()
				.equalTo("startY",endY)
				.and()
				.equalTo("endX",startX)
				.and()
				.equalTo("endY",startY)
				.endGroup()
				.findFirst();


	}


	

	//clear all objects from Book.class
	/*public void clearAll() {

		realm.beginTransaction();
		realm.clear(Book.class);
		realm.commitTransaction();
	}

	//find all objects in the Book.class


	//query a single item with the given id
	public Book getBook(String id) {

		return realm.where(Book.class).equalTo("id", id).findFirst();
	}

	//check if Book.class is empty
	public boolean hasBooks() {

		return !realm.allObjects(Book.class).isEmpty();
	}

	//query example
	public RealmResults<Book> queryedBooks() {

		return realm.where(Book.class)
				.contains("author", "Author 0")
				.or()
				.contains("title", "Realm")
				.findAll();

	}*/
}